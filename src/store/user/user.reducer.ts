import * as Types from "./action-type";

const initialState: any = {
  user: {},
  loginError: "",
  error: "",
  loading: false,
  isDoneResetPass: false,
  // resetPasswordError:"",
};

export function userReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.LOGIN_USER:
      return { ...state, loading: true };
    case Types.LOGIN_USER_SUCCESS:
      const user = action.payload;
      return { ...state, user, loading: false };
    case Types.LOGIN_USER_ERROR:
      const error = action.payload;
      return { ...state, loginError: error, loading: false };

    case Types.SINGUP_USER:
      return { ...state, loading: true };
    case Types.SINGUP_USER_SUCCESS:
      return { ...state, loading: false };
    case Types.SINGUP_USER_ERROR:
      return { ...state, error: action.payload, loading: false };

    case Types.RESET_PASSWORD:
      return { ...state, isDoneResetPass: false, loading: true };
    case Types.RESET_PASSWORD_SUCCESS:
      return { ...state, loading: false, isDoneResetPass: true };
    case Types.RESET_PASSWORD_ERROR:
      return { ...state, loading: false };

    case Types.GET_USER_SUCCESS:
      return { ...state, user: action.payload };

    case Types.LOGOUT:
      return { ...state, user: {} };

    default:
      return state;
  }
}
