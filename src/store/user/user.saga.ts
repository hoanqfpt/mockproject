import { SINGUP_USER } from "./action-type";
import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import * as Types from "../user/action-type";
import { toast } from "react-toastify";
import { loginApi, addCartWhenLoginCallAPI } from "./user.services";
import callApiCus from '../../services/axioscus.services'

const BASE_API = process.env.REACT_APP_API_URL_CUS;
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
// const addCartWhenLoginCallAPI = async (payload:any) => {
//   const { name, imageDefault, price, quantity, maSP } = payload;
//   return await callApiCus("/cart", "post", {
//     data: {
//       name,
//       imageDefault,
//       price,
//       quantity,
//       maSP,
//     },
//     headers: {
//       Authorization: "Bearer " + localStorage.getItem("accessToken"),
//     },
//   });
// };
function* login(action: any) {
  const { email, password} = action.payload.user;
  const response: IResponseGenerator = yield loginApi({
    data: {
      email: email,
      password: password,
    },
  });
  if (response.data.status !== 2000) {
    yield put({ type: Types.LOGIN_USER_ERROR, payload: response.data.message });
  } else {
    localStorage.setItem("accessToken", response.data.acessToken);
    localStorage.setItem("lastName", response.data.data.firstName);
    localStorage.setItem("idUser", response.data.data.id);
    const localCart: string | null = localStorage.getItem("cartItem");
    const listCart: string[] = JSON.parse(
      localCart !== null ? localCart : "[]"
    );
    for(let i=0;i < listCart.length;i++) {
      addCartWhenLoginCallAPI(listCart[i]);
    }
    
    localStorage.removeItem("cartItem");
    action.payload.history.push("/products");
    yield put({ type: Types.LOGIN_USER_SUCCESS, payload: response.data.data });
  }
}
const signUpUserCallAPI = async (payload: any) => {
  const { firstName, email, phone, address, password } = payload;
  return await axios.post(BASE_API + "/Register", {
    firstName,
    lastName: "",
    email,
    phone,
    address,
    password,
  });
};
function* signUpUser(action: any) {
  const response: IResponseGenerator = yield signUpUserCallAPI(
    action.payload.user
  );
  if (response.data.status !== 2000) {
    yield put({
      type: Types.SINGUP_USER_ERROR,
      payload: response.data.message,
    });
  } else {
    toast.success("Đăng ký thành công", {
      autoClose: 2000,
      className: "mt-5",
    });
    yield put({
      type: Types.SINGUP_USER_SUCCESS,
    });
    action.payload.history.push("/login");
  }
}

const resetPasswordCallAPI = async (payload: any) => {
  return await axios.post(BASE_API + "/FogotPassword", {
    Email: payload,
  });
};
function* resetPassword(action: any) {
  const response: IResponseGenerator = yield resetPasswordCallAPI(
    action.payload.email
  );
  if (response.data.status !== 2000) {
    toast.warning(response.data.message, {
      autoClose: 3000,
      className: "mt-5",
    });
    yield put({
      type: Types.RESET_PASSWORD_ERROR,
    });
  } else {
    toast.success("Reset mẩu thành công. Vui lòng kiểm tra mail.", {
      autoClose: 3000,
      className: "mt-5",
    });
    yield put({
      type: Types.RESET_PASSWORD_SUCCESS,
    });
  }
}
const confirmPasswordCallAPI = async (payload: any) => {
  const { token, password } = payload;
  debugger
  return await axios.post(
    BASE_API + "/ConfimPasword",
    {
      password: password.password,
    },
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
};
function* confirmPassword(action: any) {
  const response: IResponseGenerator = yield confirmPasswordCallAPI(
    action.payload
  );
  
  try{
 if (response.data.status !== 2000) {
   toast.warning(response.data.message, {
     autoClose: 3000,
     className: "mt-5",
   });
 } else {
   toast.success("Đổi mật khẩu thành công.", {
     autoClose: 3000,
     className: "mt-5",
   });
   action.payload.history.push("/login");
 }
  }
  catch(Error) {
debugger;
  }
 
}

const getUserCallAPI = async () => {
  return await axios.get(BASE_API + "/UserInfo", {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
};
function* getUser() {
  const response: IResponseGenerator = yield getUserCallAPI();
  if (response.data.status === 2000) {
   yield put({
     type: Types.GET_USER_SUCCESS,
     payload: response.data.data
   });
  } 
}
export function* watcherFetchUser() {
  yield takeLatest(Types.LOGIN_USER, login);
  yield takeLatest(Types.SINGUP_USER, signUpUser);
  yield takeLatest(Types.RESET_PASSWORD, resetPassword);
  yield takeLatest(Types.CONFIRM_PASSWORD, confirmPassword);
  yield takeLatest(Types.GET_USER, getUser);
}
