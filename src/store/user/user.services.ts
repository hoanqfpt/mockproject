import { AxiosRequestConfig } from "axios";
import callApiCus from "../../services/axioscus.services";

export const loginApi = async (config: AxiosRequestConfig) => await callApiCus("/login", "post", {...config});
// export const addCartWhenLoginCallAPI = async (config: AxiosRequestConfig) => await callApiCus("/cart", "post", { ...config });

export const addCartWhenLoginCallAPI = async (payload:any) => {
  const { name, imageDefault, price, quantity, maSP } = payload;
  return await callApiCus("/cart", "post", {
    data: {
      name,
      imageDefault,
      price,
      quantity,
      maSP,
    },
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
}; 