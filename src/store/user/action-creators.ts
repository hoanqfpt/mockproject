import * as Types from './action-type'
// redux saga
export const login=(user:any,history:any)=>({
    type: Types.LOGIN_USER,
    payload:{user,history}
})

export const logoutUser=()=>({
    type: Types.LOGOUT
})

export const updateUser=(user:any,history:any)=>({
    type: Types.UPDATE_USER,
    payload:{user,history}
})
export const loginUserFirebase=(payload:any)=>({
    type: Types.LOGIN_USER_FIREBASE,
    payload
})

export const signUp = (user:any,history:any) => ({ type: Types.SINGUP_USER,payload:{user,history} });
export const resetPassword = (payload: any) => ({
  type: Types.RESET_PASSWORD,
  payload,
});
export const confirmPassword = (token: string, password: any, history: any) => ({
  type: Types.CONFIRM_PASSWORD,
  payload: { token ,password,history },
});

export const getUser = () => ({
  type: Types.GET_USER
});