import {all,fork} from 'redux-saga/effects'
import * as userSaga from './user/user.saga'
import {sagaProducts} from './product/product.saga';
import {sagaCart} from './cart/cart.saga'
import {sagaAbout} from './about/about.saga'
import {sagaFavorites} from './favorites/favorites.saga'
import { sagaContact } from './admin/admin.saga';
import { sagaOrder } from './order/order.saga';
export default function* rootSaga(){
    yield all([
      fork(userSaga.watcherFetchUser),
      fork(sagaProducts),
      fork(sagaCart),
      fork(sagaAbout),
      fork(sagaFavorites),
      fork(sagaContact),
      fork(sagaOrder)
    ]);
} 