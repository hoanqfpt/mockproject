import { applyMiddleware,createStore ,compose} from "redux";
import { indexReducer } from "./index.reducer";
import createSagaMiddleware  from "@redux-saga/core";
import rootSaga from './index.saga'

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(indexReducer,composeEnhancers(applyMiddleware(sagaMiddleware)));
sagaMiddleware.run(rootSaga);
