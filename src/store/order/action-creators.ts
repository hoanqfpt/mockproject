import * as Types from "./action-type";

export const getOrder = () => ({
  type: Types.REQUEST_ORDER_LIST,
});
