import * as Types from "./action-type";

const initialState = {
  order: [],
};

export function orderReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.RECEIVE_ORDER_LIST:
      return {
        ...state,
        order: action.payload,
      };
    default:
      return state;
  }
}
