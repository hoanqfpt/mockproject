import * as Types from "./action-type";
import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";

const BASE_API = process.env.REACT_APP_API_URL;
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}

const getOrderCallApi = async () => {
  return await axios.get(BASE_API + "order");
};

function* getOrder() {
  try {
    const response: IResponseGenerator = yield getOrderCallApi();
    if (response.status === 200) {
      yield put({ type: Types.RECEIVE_ORDER_LIST, payload: response.data });
    }
  } catch {}
}

export function* sagaOrder() {
  yield takeLatest(Types.REQUEST_ORDER_LIST, getOrder);
}
