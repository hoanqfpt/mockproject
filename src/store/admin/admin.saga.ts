import * as Types from "./action-type";
import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";

const BASE_API = process.env.REACT_APP_API_URL;
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}

const getContactCallApi = async () => {
  return await axios.get(BASE_API + "contact");
};

function* getContact() {
  try {
    const response: IResponseGenerator = yield getContactCallApi();
    if (response.status === 200) {
      yield put({ type: Types.RECEIVE_CONTACT, payload: response.data });
    }
  } catch {}
}

export function* sagaContact() {
  yield takeLatest(Types.REQUEST_CONTACT, getContact);
}
