import * as Types from "./action-type";

const initialState = {
  contact: [],
};

export function contactReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.RECEIVE_CONTACT:
      return {
        ...state,
        contact: action.payload,
      };
    default:
      return state;
  }
}
