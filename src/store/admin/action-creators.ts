import * as Types from "./action-type";

export const getContact = () => ({
  type: Types.REQUEST_CONTACT,
});
