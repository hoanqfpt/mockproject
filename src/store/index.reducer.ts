import { combineReducers } from "redux";
import { userReducer } from "./user/user.reducer";
import { favoritesReducer } from "./favorites/favorites.reducer";
import {productReducer} from './product/product.reducer'
import { cartReducer } from "./cart/cart.reducer";
import { aboutReducer } from "./about/about.reducer";
import { contactReducer } from "./admin/admin.reducer";
import { orderReducer } from "./order/order.reducer";

export const indexReducer = combineReducers({
  userData: userReducer,
  favoritesData: favoritesReducer,
  productData: productReducer,
  cartData: cartReducer,
  aboutData: aboutReducer,
  contactData: contactReducer,
  orderData: orderReducer
});
