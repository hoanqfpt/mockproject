import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import * as Types from "./action-type";
import { toast } from "react-toastify";

const BASE_API = process.env.REACT_APP_API_URL;

//const USER_API = "https://614ad45f07549f001755aa4f.mockapi.io/api/v1/";
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
const getFavoritesCallAPI = async (idUser: number) => {
  const id = Number(localStorage.getItem("idUser"));
  return await axios.get(BASE_API + "/favorites?maUser=" + id);
};
function* getFavorites(action: any) {
  const response: IResponseGenerator = yield getFavoritesCallAPI(
    action.payload
  );
  yield put({ type: Types.GET_FAVORITES_SUCCESS, payload: response.data });
}

const getFavoriteByIDCallAPI = async (maSP: string) => {
  return await axios.get(BASE_API + "/favorites?maSP=" + maSP);
};

const addFavoritesCallAPI = async (payload: any) => {
 // const { nameProduct, desProduct, price, image, maSP, maUser } = payload;
  return await axios.post(BASE_API + "/favorites", payload);
};
const updateFavoritesCallAPI = async (payload: any,quantity:number,id:string) => {
  const { nameProduct, desProduct, price, image } = payload;
  return await axios.put(BASE_API + "/favorites/" + id, payload);
};
function* addFavorites(action: any) {
   const res: IResponseGenerator = yield getFavoriteByIDCallAPI(
     action.payload.maSP
   );
   const resPR = res.data.find((item: any) => item.maUser === action.payload.maUser);
   if(!!resPR){
      // const response: IResponseGenerator = yield updateFavoritesCallAPI(
      //   action.payload,
      //   resPR.quantity,
      //   resPR.id
      // );
       toast.warning("Sản phẩm đã có trong danh sách yêu thích!", {
         autoClose: 2000,
         className: "mt-5",
       });   
   }
   else{
     const response: IResponseGenerator = yield addFavoritesCallAPI(
       action.payload
     );
      toast.success("Đã thêm vào danh sách yêu thích!", {
        autoClose: 2000,
        className: "mt-5",
      });
   }
 
  yield put({ type: Types.ADD_FAVORITES_SUCCESS });
}

const deleteFavoritesCallAPI = async (id: number) => {
  return await axios.delete(BASE_API + "/favorites/" + id);
};
function* deleteFavorites(action: any) {
  const response: IResponseGenerator = yield deleteFavoritesCallAPI(
    action.payload
  );
  toast.success("Xóa thành công!", {
    autoClose: 2000,
    className: "mt-5",
  });
  yield put({ type: Types.DELETE_FAVORITES_SUCCESS });
}

export function* sagaFavorites() {
  yield takeLatest(Types.GET_FAVORITES, getFavorites);
  yield takeLatest(Types.ADD_FAVORITES, addFavorites);
  yield takeLatest(Types.DELETE_FAVORITES, deleteFavorites);
}
