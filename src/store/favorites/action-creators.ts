import * as Types from './action-type'

// redux saga
export const getFavorites=()=>({
    type: Types.GET_FAVORITES,
})
export const addFavorites = (payload: any) => ({
  type: Types.ADD_FAVORITES,
  payload,
});
export const deleteFavorites = (payload: any) => ({
  type: Types.DELETE_FAVORITES,
  payload,
});