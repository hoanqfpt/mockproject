import * as Types from "./action-type";

const initialState: any = {
  favorites: [],
  loading:false,
};

export function favoritesReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.GET_FAVORITES_SUCCESS:
      const favorites = action.payload;
      return { ...state, favorites };
    case Types.ADD_FAVORITES:
      return { ...state, loading: true };
    case Types.ADD_FAVORITES_SUCCESS:
      return { ...state, loading: false };
    case Types.DELETE_FAVORITES:
      return { ...state, loading: true };
    case Types.DELETE_FAVORITES_SUCCESS:
      return { ...state, loading: false };
    default:
      return state;
  }
}
