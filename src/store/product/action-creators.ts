import * as Types from './action-type'

export const getData = (payload:any)=>({  type: Types.GET_DATA,  payload})

export const searchData = (payload:string)=>({  type: Types.SEARCH_DATA,   payload})

export const getCategories = ()=>({   type: Types.GET_CATEGORIES})

export const getDataByCateID = (payload:string)=>({   type: Types.GET_DATA_BY_CATEID, payload})

export const getDataBySize = (payload:string)=>({   type: Types.GET_DATA_BY_SIZE,    payload})

export const getDataBySizeRedux = (payload:string)=>({ type: Types.GET_DATA_REDUX_FILTER_SIZE, payload});

export const getDataByPrice = (payload:number[])=>({   type: Types.GET_DATA_BY_PRICE,  payload})

export const sortData = (payload:string)=>({ type: Types.SORT_DATA, payload})

export const getDataById = (payload: string) => ({type: Types.GET_DATA_BY_ID, payload})
export const getProductSalesInHome = (payload: string) => ({
  type: Types.GET_PRODUCT_SALE_HOME
});