import * as Types from './action-type';
import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";
// import callApi from '../../services/axios.services'

const BASE_API = "https://614ad45f07549f001755aa4f.mockapi.io/api/v1";
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
const getProductsCallAPI = async () => {
  return await axios.get(BASE_API + "/products");
};
function* getProducts(action: any) {
  try {
    const response: IResponseGenerator = yield getProductsCallAPI();
    // yield callApi("/products","GET");
    yield put({ type: Types.GET_DATA_SUCCESS, payload: response.data });
  } catch {
    //yiel put error
  }
}
const getCategoriesCallAPI = async () => {
   return await axios.get(BASE_API + "/categories");
};
function* getCategories() {
  try {
    const response: IResponseGenerator = yield getCategoriesCallAPI();
    yield put({ type: Types.GET_CATEGORIES_SUCCESS, payload: response.data });
  } catch {
    //yiel put error
  }
}

const getDataByIdCallApi = async (id: string) => {
  return await axios.get(BASE_API + '/products/' + id);
}

function* getDataById(action: any) {
  try{
    const response: IResponseGenerator = yield getDataByIdCallApi(action.payload);
    yield put({type: Types.GET_DATA_BY_ID_SUCCESS, payload: response.data})
  } catch{}
}

const getDataByCateIDCallAPI = async (id:string) => {
  return await axios.get(BASE_API + "/products?categoryID="+id);
};
function* getDataByCateID(action:any) {
  try {
    // if(action.payload !== ""){
      const response: IResponseGenerator = yield getDataByCateIDCallAPI(action.payload);
       yield put({ type: Types.GET_DATA_BY_CATEID_SUCCESS, payload: response.data });
    // }
   
  } catch {
    //yiel put error
  }
}

//get products by size 
const getDataBySizeCallAPI = async (size:string) => {
  return await axios.get(BASE_API + "/products?size="+size);
};
function* getDataBySize(action:any) {
  try {
      const response: IResponseGenerator = yield getDataBySizeCallAPI(action.payload);
      yield put({ type: Types.GET_DATA_BY_SIZE_SUCCESS, payload: response.data });
   
  } catch {
    //yiel put error
  }
}

//get products by size 
const searchDataCallAPI = async (txtSearch:string) => {
  return await axios.get(BASE_API + "/products?search="+txtSearch);
};
function* searchData(action:any) {
  try {
      const response: IResponseGenerator = yield searchDataCallAPI(action.payload);
      yield put({ type: Types.SEARCH_DATA_SUCCESS, payload: response.data });
   
  } catch {
    //yiel put error
  }
}
export function* sagaProducts() {
  yield takeLatest(Types.GET_DATA, getProducts);
  yield takeLatest(Types.GET_CATEGORIES, getCategories);
  yield takeLatest(Types.SEARCH_DATA, searchData);
  yield takeLatest(Types.GET_DATA_BY_ID, getDataById);
  // yield takeLatest(Types.GET_DATA_BY_ID, getDataById);
}
