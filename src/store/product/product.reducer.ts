import * as Types from "./action-type";

const initialState = {
  products: [],
  productsFilter: [],
  categories: [],
  isFilterCate: false,
  isFilterSize: false,
  isFilterPrice: false,
  categoryID: "",
  size: "",
  price: [],
  searchData: [],
  productId: []
};
const getDataFilter = (
  productsFilter: string[],
  categoryID: string,
  size: string,
  price: number[]
) => {
  const productsby = productsFilter.filter((item: any) => {
    let check = false;
    item.size.forEach((itemSize: any) => {
      if (itemSize === Number(size)) {
        check = true;
        return;
      }
    });
    if (
      (check || size === "") &&
      (categoryID === item.categoryID || categoryID === "") &&
      ((item.price >= price[0] && item.price <= price[1]) || price.length === 0)
    ) {
      return item;
    }
  });
  return productsby;
};

const sortData = (data: string[], type: string) => {};

export function productReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.GET_DATA_SUCCESS:
      const products = action.payload;
      return {
        ...state,
        products,
        productsFilter: products,
        isFilterCate: false,
        isFilterSize: false,
        isFilterPrice: false,
        categoryID: "",
        size: "", 
        price: [],
      };
    case Types.GET_CATEGORIES_SUCCESS:
      const categories = action.payload;
      return { ...state, categories };

    // case Types.GET_DATA_BY_CATEID_SUCCESS:
    //   const categoriesbyCate = action.payload;
    //   return {
    //     ...state,
    //     products: categoriesbyCate,
    //     productsByCate: categoriesbyCate,
    //     isFilterCate: true,
    //     isFilterSize: false,
    //   };

    // case Types.GET_DATA_BY_SIZE_SUCCESS:
    //   const categoriesBySize = action.payload;
    //   return { ...state, products: categoriesBySize, isFilterSize: true };
    case Types.GET_DATA_BY_CATEID:
      const categoriesbyCate = getDataFilter(
        state.productsFilter,
        action.payload,
        state.size,
        state.price
      );
      return {
        ...state,
        products: categoriesbyCate,
        isFilterCate: true,
        categoryID: action.payload,
      };

    case Types.GET_DATA_REDUX_FILTER_SIZE:
      const productsBySize = getDataFilter(
        state.productsFilter,
        state.categoryID,
        action.payload,
        state.price
      );
      return {
        ...state,
        products: productsBySize,
        isFilterSize: true,
        size: action.payload,
      };

    case Types.GET_DATA_BY_PRICE:
      const categoriesbyPrice = getDataFilter(
        state.productsFilter,
        state.categoryID,
        state.size,
        action.payload
      );
      return {
        ...state,
        products: categoriesbyPrice,
        isFilterPrice: true,
        price: action.payload,
      };

    case Types.SEARCH_DATA_SUCCESS:
      const searchData = action.payload;
      return {
        ...state,
        searchData,
      };
    case Types.SORT_DATA:
      if (action.payload === "new") {
        const products = state.products.sort((a: any, b: any) => {
          return a.dateCreate - b.dateCreate;
        });
        return {
          ...state,
          products,
        };
      } else if (action.payload === "minmax") {
        state.products.sort((a: any, b: any) => {
          return a.price - b.price;
        });
        return {
          ...state,
        };
      } else if (action.payload === "maxmin") {
        state.products.sort((a: any, b: any) => {
          return b.price - a.price;
        });
        return {
          ...state,
        };
      } else {
        return {
          ...state,
        };
      }

      case Types.GET_DATA_BY_ID_SUCCESS:
        const productId = action.payload;
        return {
          ...state,
          productId,
        }

    default:
      return state;
  }
}
