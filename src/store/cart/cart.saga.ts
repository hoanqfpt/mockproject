import * as Types from "./action-type";
import { put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import { deleteCartByUserAPI } from "./cart.services";
import callApiCus from "../../services/axioscus.services";

const BASE_API = process.env.REACT_APP_API_URL_CUS;
interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
const addCartCallAPI = async (payload: any) => {
  return await axios.post(BASE_API + "/cart", payload, {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
};
function* addCart(action: any) {
  try {
    const response: IResponseGenerator = yield addCartCallAPI(action.payload);
    yield put({
      type: Types.ADD_TO_CART_API_SUCESS,
      payload: response.data.data,
    });
  } catch {}
}

const getCartCallAPI = async () => {
  return await axios.get(BASE_API + "/cart", {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
};
function* getCart(action: any) {
  try {
    const response: IResponseGenerator = yield getCartCallAPI();
    if (response.data.status === 2000) {
      yield put({
        type: Types.GET_CART_API_SUCCESS,
        payload: response.data.data,
      });
    }
  } catch {}
}
const updateQuantityCartCallAPI = async (payload: any) => {
  return await axios.post(BASE_API + "/updateQuantityCart", payload, {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
};
function* updateQuantityCart(action: any) {
  try {
    const response: IResponseGenerator = yield updateQuantityCartCallAPI(
      action.payload
    );
    if (response.data.status === 2000) {
      yield put({
        type: Types.GET_CART_API_SUCCESS,
        payload: response.data.data,
      });
    }
  } catch {}
}
const deleteCartCallAPI = async (payload: any) => {
  return await axios.post(
    BASE_API + "/deleteCart",
    { maSP: payload },
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
};
function* deleteCart(action: any) {
  try {
    const response: IResponseGenerator = yield deleteCartCallAPI(
      action.payload
    );
    if (response.data.status === 2000) {
      yield put({
        type: Types.DELETE_CART_API_SUCCESS,
        payload: response.data.data,
      });
    }
  } catch {}
}
function* deleteCartByUser(action: any) {
  try {
    const response: IResponseGenerator = yield deleteCartByUserAPI();
    if (response.data.status === 2000) {
      yield put({
        type: Types.DELETE_CART_BY_USER_API_SUCCESS,
      });
      action.payload.push("/checkoutsuccess");
    }
  } catch {}
}

export function* sagaCart() {
  yield takeLatest(Types.ADD_TO_CART_API, addCart);
  yield takeLatest(Types.GET_CART_API, getCart);
  yield takeLatest(Types.UPDATE_QUANTITY_CART_API, updateQuantityCart);
  yield takeLatest(Types.DELETE_CART_API, deleteCart);
  yield takeLatest(Types.DELETE_CART_BY_USER_API, deleteCartByUser);
}
