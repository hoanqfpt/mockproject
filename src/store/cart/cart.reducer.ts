import * as Types from "./action-type";

const initialState = {
  carts: [],
  loading:false,
};
export function cartReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.ADD_TO_CART:
      return { ...state, carts: action.payload };
    case Types.GET_CART_STORAGE:
      return { ...state, carts: action.payload };
    case Types.GET_CART_API_SUCCESS:
      return { ...state, carts: action.payload };

    case Types.ADD_TO_CART_API:
      return { ...state, loading: true };
    case Types.ADD_TO_CART_API_SUCESS:
      return { ...state, carts: action.payload, loading: false };

    case Types.DELETE_CART_API_SUCCESS:
      return { ...state, carts: action.payload };
    case Types.DELETE_CART_BY_USER_API_SUCCESS:
      return { ...state, carts: [] };

    case Types.DELETE_CART_STORAGE:
      return { ...state, carts: [] };
    default:
      return state;
  }
}
