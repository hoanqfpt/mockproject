import callApiCus, { get } from "../../services/axioscus.services";
export const getCartAPI = (payload: any) => get(payload.url, payload.config);
export const deleteCartByUserAPI = async () => {
  return await callApiCus("/deleteCartByUser", "post", {
    data:{},
    headers: {
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
  });
}; 