import * as Types from './action-type'

export const addToCart = (payload:any)=>({
    type: Types.ADD_TO_CART,
    payload
})
export const addToCartAPI = (payload: any) => ({
  type: Types.ADD_TO_CART_API,
  payload,
});

export const getCartStorages = (payload: any) => ({
  type: Types.GET_CART_STORAGE,
  payload,
});
export const getCartAPI = (payload: any) => ({
  type: Types.GET_CART_API,
  payload,
});

export const updateQuantityCartAPI = (payload: any) => ({
  type: Types.UPDATE_QUANTITY_CART_API,
  payload,
});
export const deleteCartAPI = (payload: any) => ({
  type: Types.DELETE_CART_API,
  payload,
});
export const deleteCartAPIByUser = (payload:any) => ({
  type: Types.DELETE_CART_BY_USER_API,
  payload
});
export const deleteAllCartStorage = () => ({
  type: Types.DELETE_CART_STORAGE
});