import * as Types from "./action-type";
import { put, takeLatest } from "@redux-saga/core/effects";
import axios from "axios";
// import callApi from "../../services/axios.services";

const BASE_API = "https://614ad45f07549f001755aa4f.mockapi.io/api/v1";

interface IResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}

const callApiIntro = axios.get(BASE_API + "/about");
const callApiReason = axios.get(BASE_API + "/about/1/about_box");

const getIntroCallApi = async () => {
  return await callApiIntro;
};

function* getIntro() {
  try {
    const response: IResponseGenerator = yield getIntroCallApi();
    yield put({ type: Types.RECEIVE_INTRO, payload: response.data });
  } catch {}
}

const getReasonCallApi = async () => {
  return await callApiReason;
};

function* getReason() {
  try {
    const response: IResponseGenerator = yield getReasonCallApi();
    yield put({ type: Types.RECEIVE_REASON, payload: response.data });
  } catch {}
}

export function* sagaAbout() {
  yield takeLatest(Types.REQUEST_INTRO, getIntro);
  yield takeLatest(Types.REQUEST_REASON, getReason);
}
