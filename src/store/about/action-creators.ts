import * as Types from "./action-type";

export const requestIntro = () => ({ type: Types.REQUEST_INTRO });

export const requestReason = () => ({ type: Types.REQUEST_REASON });
