import * as Types from "./action-type";

const initialState = {
  intro: [],
  reason: [],
};

export function aboutReducer(state = initialState, action: any) {
  switch (action.type) {
    case Types.RECEIVE_INTRO:
      const intro = action.payload;
      return {
        ...state,
        intro,
      };
    case Types.RECEIVE_REASON:
      const reason = action.payload;
      return {
        ...state,
        reason,
      };
    default:
      return state;
  }
}
