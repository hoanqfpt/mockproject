import React, { useEffect } from "react";
import BoxProductComponent from "../components/products/BoxProductComponent";
import HeaderProduct from "../components/products/HeaderProduct";
import { useDispatch, useSelector } from "react-redux";
import { searchData } from "../store/product/action-creators";
import { useParams } from "react-router-dom";

function Search() {
  const dispatch = useDispatch();
  const param: any = useParams();
  useEffect(() => {
    const action = searchData(param.keyword);
    dispatch(action);
  }, [param]);
  const data = useSelector((state: any) => state.productData.searchData);
  return (
    <React.Fragment> 
        
      <div className="container">
        <HeaderProduct />
       
        <div className="row">
          <div className="col-md-12">
            <div className="row">
              {data &&
                data.map((item: any) => {
                  return (
                    <div className="col-md-3" key={item.id}>
                      <BoxProductComponent {...item} />
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default Search;
