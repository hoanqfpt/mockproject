import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
 import UserAccount from "../components/account/UserAccount";
 import {getUser} from '../store/user/action-creators'
function Account() {
const dispatch = useDispatch();
const user = useSelector((state: any) => state.userData.user);
useEffect(() => {
  dispatch(getUser());
},[])
  return (
    <React.Fragment>
      <UserAccount data = {user} />
    </React.Fragment>
  );
}
export default Account;
