import React from "react";
import { useSelector, useDispatch } from "react-redux";
import BoxFavoritesComponent from "../components/favorites/BoxFavoritesComponent";
import { getFavorites } from "../store/favorites/action-creators";

function Favorites() {
  const loading = useSelector((state: any) => state.favoritesData.loading);
  const dispatch = useDispatch();
  const dataFavorites = useSelector(
    (state: any) => state.favoritesData.favorites
  );
  React.useEffect(() => {
    dispatch(getFavorites());
  }, [loading]);

  return (
    <React.Fragment>
      {loading ? (
        <div className="d-flex justify-content-center loadFavorites">
          <div className="spinner-grow text-danger" role="status"></div>
        </div>
      ) : null}

      <div  className={`boxFavorites container mt-5 w-50 ${loading && "contentFavorites"}`}>
        {dataFavorites &&
          dataFavorites.map((item: any) => {
            return <BoxFavoritesComponent {...item} key={item.id} />;
          })}
      </div>
    </React.Fragment>
  );
}
export default Favorites;
