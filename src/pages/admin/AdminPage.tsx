import { Menu } from "antd";
import React from "react";
import { MailFilled, TagsFilled } from "@ant-design/icons";
import {
  BrowserRouter as Router,
  Link,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import AdminOrderList from "./AdminOrderList";
import ContactList from "./ContactList";

const AdminPage = () => {
  return (
    <Router>
      <Redirect from="/admin" to="/admin/admin-contact" />
      <Menu mode="horizontal" defaultSelectedKeys={["contact"]}>
        <Menu.Item
          key="contact"
          icon={<MailFilled />}
          className="d-flex align-items-center"
        >
          <Link to="/admin/admin-contact" className="text-decoration-none">
            Contact
          </Link>
        </Menu.Item>

        <Menu.Item
          key="order"
          icon={<TagsFilled />}
          className="d-flex align-items-center"
        >
          <Link to="/admin/admin-order" className="text-decoration-none">
            Order
          </Link>
        </Menu.Item>
      </Menu>

      <div className="mt-5">
        <Switch>
          <Route path="/admin/admin-contact">
            <ContactList />
          </Route>
          <Route path="/admin/admin-order">
            <AdminOrderList />
          </Route>
          {/* <Route path="/home">
            <Home />
          </Route> */}
        </Switch>
      </div>
    </Router>
  );
};

export default AdminPage;
