import { Table } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getOrder } from "../../store/order/action-creators";
import More from "./More";

const AdminOrderList = () => {
  const dispatch = useDispatch();
  useEffect(() => {
      const maUser = localStorage.getItem("idUser");
      if (maUser === "1") {
         dispatch(getOrder());
      }
   
  }, []);
  const state = useSelector((state: any) => state.orderData);
  const { order } = state;

  const title = [
    "Thứ tự",
    "Ngày",
    "Tên khách hàng",
    "Địa chỉ",
    "E-mail",
    "Điện thoại",
    "Tổng tiền",
    "Chi tiết sản phẩm",
  ];

  const detailTitle = ["Thứ tự", "Tên sản phẩm", "Số lượng", "Tổng tiền"];

  const columns = title.map((v: string) => {
    return {
      title: v,
      dataIndex: v,
      key: v,
    };
  });

  const detailColumns = detailTitle.map((v: string) => {
    return {
      title: v,
      dataIndex: v,
      key: v,
    };
  });

  const data = order?.sort(function (a: any, b: any) {
   // return b.dateCreate - a.dateCreate;
                  let x = a.dateCreate;
                  let y = b.dateCreate;
                  if (x < y) {
                    return 1;
                  }
                  if (x > y) {
                    return -1;
                  }
                  return 0;
                }).map((v: any, i: number) => {
    return {
      key: v.id,
      "Thứ tự": i + 1,
      Ngày: new Date(v.dateCreate).toUTCString(),
      "Tên khách hàng": v.nameCustomer,
      "Địa chỉ": v.address,
      "E-mail": v.email,
      "Điện thoại": v.phone,
      "Tổng tiền": v.total,
      "Chi tiết sản phẩm": (
        <More
          message={
            <Table
              dataSource={v.products.map((v: any, i: number) => {
                return {
                  key: v.nameProduct,
                  "Thứ tự": i + 1,
                  "Tên sản phẩm": v.nameProduct,
                  "Số lượng": v.quantity,
                  "Tổng tiền": `${v.quantity * v.price} đ`,
                };
              })}
              columns={detailColumns}
              pagination={{ position: ["bottomCenter"] }}
            />
          }
        />
      ),
    };
  });

  return (
    <>
      <Table
        dataSource={data}
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
      />
    </>
  );
};

export default AdminOrderList;
