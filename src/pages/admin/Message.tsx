import React from "react";
import { Modal } from "react-bootstrap";
import ReactDOM from "react-dom";

interface IMessageProps {
  handleClose: () => void;
  show: boolean;
  message: string | React.ReactElement;
}

const Message = (Props: IMessageProps) => {
  const { show, handleClose, message } = Props;
  return ReactDOM.createPortal(
    show ? (
      <Modal show={show} onHide={handleClose}>
        <Modal.Body>{message}</Modal.Body>
      </Modal>
    ) : null,
    document.body
  );
};

export default Message;
