import { Button } from "antd";
import React, { useState } from "react";
import ButtonComponent from "../../common-ui/ButtonComponent";
import Message from "./Message";

interface IMoreProps {
  message: string | React.ReactElement;
}

const More = (Props: IMoreProps) => {
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);
  return (
    <>
      <Message handleClose={handleClose} show={show} message={Props.message} />
      <ButtonComponent onClick={handleShow} name="Xem thêm" size="sm" color="info" />
    </>
  );
};

export default More;
