import { Table } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getContact } from "../../store/admin/action-creators";
import More from "./More";

const ContactList = () => {
  const dispatch = useDispatch();
  const state = useSelector((state: any) => state.contactData);
  const { contact } = state;

  useEffect(() => {
    dispatch(getContact());
  }, []);

  const title = ["Thứ tự", "Tên khách hàng", "E-mail", "Điện thoại", "Tin nhắn"];
  const data = contact?.map((v: any, i: number) => {
    return {
      key: v.id,
      "Thứ tự": i + 1,
      "Tên khách hàng": v.name,
      "E-mail": v.email,
      "Điện thoại": v.phone,
      "Tin nhắn": <More message={v.message} />,
    };
  });

  const columns = title.map((v: string) => {
    return {
      title: v,
      dataIndex: v,
      key: v,
    };
  });

  return (
    <>
      <Table
        dataSource={data}
        columns={columns}
        pagination={{ position: ["bottomCenter"] }}
      />
      ;
    </>
  );
};

export default ContactList;
