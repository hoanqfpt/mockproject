import React from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { Container } from "react-bootstrap";
import ButtonComponent from "../../common-ui/ButtonComponent";
import styles from "./AdminLogin.module.css";
import { useHistory } from "react-router";

const AdminLogin = () => {
  const DisplayError = Yup.object().shape({
    username: Yup.string().required("Username is required"),
    password: Yup.string().required("Password is required"),
  });
  const history = useHistory();
  const initialValues = {
    username: "",
    password: "",
  };
  return (
    <React.Fragment>
      <Container fluid className={styles["form-container"]}>
        <Formik
          initialValues={initialValues}
          validationSchema={DisplayError}
          onSubmit={(values) => {
            history.push('/admin-contact')
          }}
        >
          {({ errors, touched }) => (
            <Form className="w-25 text-left border rounded py-5 px-3 shadow">
              <div className="form-group">
                <h2 className="text-center mb-4">Admin</h2>
                <Field
                  type="text"
                  id="username"
                  name="username"
                  placeholder="Nhập username"
                  className="form-control"
                />
                {touched.username && errors.username && (
                  <div className="text-danger">{errors.username}</div>
                )}
              </div>
              <div className="form-group">
                <Field
                  type="password"
                  id="password"
                  name="password"
                  placeholder="Nhập mật khẩu"
                  className="form-control"
                />
                {touched.password && errors.password && (
                  <div className="text-danger">{errors.password}</div>
                )}
              </div>
              <ButtonComponent name="Login" type="submit" />
            </Form>
          )}
        </Formik>
      </Container>
    </React.Fragment>
  );
};

export default AdminLogin;
