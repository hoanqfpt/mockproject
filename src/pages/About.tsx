import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import style from "./css/About.module.css";
import { requestIntro, requestReason } from "../store/about/action-creators";
import ReasonComponent from "../components/about/ReasonComponent";
import AboutHeader from "../components/about/AboutHeader";

interface INTRO {
  description: string;
  id: string;
}

interface REASON {
  title: string;
  imgLink: string;
  description: string;
  aboutId: string;
  id: string;
}

function About() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(requestIntro());
    dispatch(requestReason());
  }, []);
  const state = useSelector((state: any) => state.aboutData);
  const { intro, reason } = state;
  return (
    <React.Fragment>
      <AboutHeader title="Giới thiệu" />

      <section className={`${style.main} container-fluid px-0`}>
        <div className={`${style.intro} container`}>
          {intro.map((v: INTRO, i: number) => {
            return <p key={i}>{v.description}</p>;
          })}
        </div>

        <div className={`${style.reasons} container-fluid`}>
          {reason.map((v: REASON, i: number) => {
            return (
              <ReasonComponent
                title={v.title}
                content={v.description}
                img={v.imgLink}
                key={i}
                id={i}
              />
            );
          })}
        </div>
      </section>
    </React.Fragment>
  );
}
export default About;
