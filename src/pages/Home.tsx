import React, { useEffect } from "react";
import BoxItem from "../components/home/BoxItem";
import DividerComponent from "../common-ui/DividerComponent";
import { useDispatch, useSelector } from "react-redux";
import { getData } from "../store/product/action-creators";

function Header() {
  const data = useSelector((state: any) => state.productData.products);
  const dataSale = () => {
      return data
        .filter((item: any) => {
          return item.price < item.oldPrice;
        })
        .slice(0, 4);
  };
   const dataNew = () => {
       return data
         .sort(function (a: any, b: any) {
           return a.dateCreate - b.dateCreate;
         }).slice(0, 4);
   };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getData(1));
  }, []);
  return (
    <React.Fragment>
      <section className="mainSlide">
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to="0"
              className="active"
            ></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src="https://theme.hstatic.net/1000333436/1000696015/14/slideshow_1.jpg?v=345"
                className="d-block w-100"
                alt="giày da"
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://theme.hstatic.net/1000333436/1000696015/14/slideshow_2.jpg?v=460"
                className="d-block w-100"
              ></img>
            </div>
          </div>
        </div>
      </section>
      <div className="container mt-5 ">
        <div className="row">
          <div className="col-md-6">
            <img
              className="w-100"
              src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_1_1024x1024.png?v=345"
            ></img>
          </div>
          <div className="col-md-6 ">
            <div className="row">
              <div className="col-md-12 d-flex">
                <div className="row">
                  <div className="col-md-6">
                    <img
                      className="w-100"
                      src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_2_1024x1024.png?v=345"
                    ></img>
                  </div>
                  <div className="col-md-6">
                    <img
                      className="w-100"
                      src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_3_1024x1024.png?v=345"
                    ></img>
                  </div>
                </div>
              </div>
              <div className="col-md-12 mt-3">
                <img
                  className="w-100"
                  src="https://theme.hstatic.net/1000333436/1000696015/14/hc_img_4_1024x1024.png?v=345"
                ></img>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt-5 mb-5 w-75">
        <DividerComponent name="Sản phẩm giảm giá" className="text-danger" />
        <div className="row">
          {dataSale() &&
            dataSale().map((item: any) => {
              return <BoxItem key={item.id} {...item} />;
            })}
        </div>
        <DividerComponent name="Sản phẩm mới" className="text-danger" />
        <div className="row">
          {dataNew() &&
            dataNew().map((item: any) => {
              return <BoxItem key={item.id} {...item} />;
            })}
        </div>
      </div>
    </React.Fragment>
  );
}
export default Header;
