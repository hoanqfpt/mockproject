import React from "react";
import Header from "../components/Header";
import { useHistory } from "react-router-dom";

function PageNotFound() {
  const history = useHistory();
  const backHome = () => {
    history.push("/home");
  };
  return (
    <React.Fragment>
      <div className="pagenofound mt-5">
        <div className="container vh-100 mt-5">
          <div className="row">
            <div className="text-center m-auto">
                <h1>404 - Page Not Found</h1>
                <button className="btn btn-success" onClick={backHome}>Back home</button>
                </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default PageNotFound;
