import React, { useEffect, useState } from "react";
import BoxProductComponent from "../components/products/BoxProductComponent";
import CategoryComponent from "../components/products/filter/CategoryComponent";
import SizeComponent from "../components/products/filter/SizeComponent";
import PriceComponent from "../components/products/filter/PriceComponent";
import HeaderProduct from "../components/products/HeaderProduct";
import Pagination from "../common-ui/Pagination";
import { useDispatch, useSelector } from "react-redux";
import { getData } from "../store/product/action-creators";
import ButtonComponent from "../common-ui/ButtonComponent";
import { useSliceData } from "../common-ui/usePagination";

function Products() {
  const dispatch = useDispatch();
  const state = useSelector((state: any) => state.productData);
  const { isFilterCate, isFilterSize, isFilterPrice, products, categories } =
    state;
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(4);

  const sliceData = useSliceData({
    products,
    itemsPerPage: 9,
    currentPage: page,
  });

  useEffect(() => {
    const total = Math.round(products.length / 9);
    setTotalPage(total);
  }, [products]);
  useEffect(() => {
    const action = getData(page);
    dispatch(action);
  }, []);
  const getNameCategory = (categoryId: string) => {
    const category = categories.filter((item: any) => {
      return item.id === categoryId;
    });
    if (category.length > 0) return category[0].name;
  };

  const onClickchangePage = (e: any) => {
    setPage(e);
  };

  const onClickRemoveFilter = () => {
    setPage(1);
    const action = getData(1);
    dispatch(action);
  };
  // console.log(data)
  return (
    <React.Fragment>
      <div className="container">
        {/* <HeaderProduct /> */}
        <div className="row">
          <div className="col-md-3">
            <CategoryComponent />
            <SizeComponent />
            <PriceComponent />
            {(isFilterCate || isFilterSize || isFilterPrice) && (
              <ButtonComponent
                name="Xóa filter"
                onClick={onClickRemoveFilter}
              />
            )}
          </div>
          <a href=""></a>
          <div className="col-md-9">
            <div className="row">
              {sliceData &&
                sliceData.map((item: any) => {
                  const categoryName = getNameCategory(item.categoryID);
                  return (
                    <div className="col-md-4" key={item.id}>
                      <BoxProductComponent
                        {...item}
                        categoryName={categoryName}
                      />
                    </div>
                  );
                })}
            </div>
            <div className="row">
              {/* <nav aria-label="Page navigation example">
                <ul className="pagination">
                  <li className={`page-item`}>
                    <a className="page-link" href="#" onClick={prevPage}>
                      Previous
                    </a>
                  </li>

                  {pagination.map((item: any) => {
                    if (!item.ellipsis) {
                      return (
                        <li
                          key={item.id}
                          className={
                            item.current ? "page-item  active" : "page-item "
                          }
                        >
                          <a
                            className="page-link"
                            href="#"
                            onClick={(e) => changePage(item.id, e)}
                          >
                            {item.id}
                          </a>
                        </li>
                      );
                    } else {
                      return (
                        <li key={item.id}>
                          <span className="pagination-ellipsis">&hellip;</span>
                        </li>
                      );
                    }
                  })}
                  <li className={`page-item`}>
                    <a className="page-link" href="#" onClick={nextPage}>
                      Next
                    </a>
                  </li>
                </ul>
              </nav> */}

              <Pagination  totalPage={totalPage}  changePage={onClickchangePage}/>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default Products;
