import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import style from "./css/Cart.module.css";
import AboutHeader from "../components/about/AboutHeader";
import CartTable from "../components/cart/CartTable";
import ButtonComponent from "../common-ui/ButtonComponent";
import { NavLink } from "react-router-dom";
import { CART } from "../components/cart/CartItem";
import { getCartStorages, getCartAPI } from "../store/cart/action-creators";

function Cart() {
  const history = useHistory();
  const dataCart = useSelector((state: any) => state.cartData.carts);
  const dispatch = useDispatch();
  const localCart: string | null = localStorage.getItem("cartItem");
  const listCart: CART[] = JSON.parse(localCart !== null ? localCart : "[]");
  useEffect(() => {
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      dispatch(getCartAPI(maUser));
    } else {
      dispatch(getCartStorages(listCart));
    }
  }, []);
  return (
    <React.Fragment>
      <AboutHeader title="Giỏ hàng" />

      <section
        style={{ minHeight: "35vh" }}
        className={`mt-5 ${style["your-cart"]} container-fluid`}
      >
        {dataCart.length > 0 || listCart.length > 0 ? (
          <>
            <CartTable />
            <div className="text-right">
              <ButtonComponent
                name="Tiếp tục mua hàng"
                type="button"
                color="dark"
                className="mx-2"
                onClick={() => {
                  history.push("/products");
                }}
              />
              <ButtonComponent
                name="Thanh toán"
                type="button"
                color="success"
                onClick={() => {
                  history.push("/checkout");
                }}
              />
            </div>
          </>
        ) : (
          <h5 className="pt-5">
            Giỏ hàng của bạn đang trống. Mua hàng{" "}
            <NavLink to="/products">tại đây</NavLink>
          </h5>
        )}
      </section>
    </React.Fragment>
  );
}
export default Cart;
