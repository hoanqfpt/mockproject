import React from "react";
import MainContentComponent from "../components/news/MainContentComponent";

function News() {
  return (
    <React.Fragment>
      <MainContentComponent />
    </React.Fragment>
  );
}
export default News;
