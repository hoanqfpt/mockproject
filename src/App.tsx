import React from "react";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import CheckOutSuccess from "./components/checkoutpage/checkoutcomponents/CheckOutSuccess";
import PageNotFound from "./pages/PageNotFound"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import Search from "./pages/Search";
import { useSelector } from "react-redux";
import routes from "./routes";
import BreadCrumbs from "./components/breadcrum/BreadCrumbs";
import LookBook from "./components/lookbook/LookBook"
function App() {
  const dataUser = useSelector((state: any) => state.userData.user);
  const nameUser = localStorage.getItem("lastName");
  const exclusiveArray = ["/admin-login", "/admin-contact", "/admin-order"];
  return (
    <Router>
      <React.Fragment>
        <Header />
        {/* {exclusiveArray.indexOf(location.pathname) < 0 && <Header />} */}
        <div className="mt-5">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/home" component={Home} />
            <Route path="/search/:keyword" component={Search} />
            <Route path="/checkoutsuccess" component={CheckOutSuccess} />
            <Route path="/lookbook" component={LookBook} />

            {routes.map(({ path, name, Component, authen }, key) => (
              <Route
                exact
                path={path}
                key={key}
                render={(props) => {
                  const crumbs = routes

                    .filter(({ path }) => props.match.path.includes(path))

                    .map(({ path, ...rest }) => ({
                      path: Object.keys(props.match.params).length
                        ? Object.keys(props.match.params).reduce(
                            (path: any, param: any) =>
                              path.replace(
                                `:${param}`,
                                props.match.params[param]
                              ),
                            path
                          )
                        : path,
                      ...rest,
                    }));

                  return (
                    <div>
                      <BreadCrumbs crumbs={crumbs} />
                      {!authen ? ( //@ts-ignore
                        <Component {...props} />
                      ) : dataUser && nameUser !== null ? (
                        <Redirect to="/" />
                      ) : (
                        //@ts-ignore
                        <Component {...props} />
                      )}
                    </div>
                  );
                }}
              />
            ))}
            <Route component={PageNotFound}></Route>
          </Switch>
        </div>
        {exclusiveArray.indexOf(location.pathname) < 0 && <Footer />}
      </React.Fragment>
    </Router>
  );
}

export default withRouter(App);
