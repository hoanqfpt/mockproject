import React from "react";
import styles from "./UserAccount.module.css";
import { Link } from "react-router-dom";

interface IUserAccountProps {
  data: any
}
function UserAccount(Props: IUserAccountProps) {
  console.log(Props)
  const {data} = Props;
  return (
    <React.Fragment>
      <div className="bg-info">
        <div className="container bg-light py-3 ">
          <div className="row mx-5 py-4 border-bottom">
            <div className="col-md-6">
              <div className="d-flex align-items-center" id={styles.userTitle}>
                <img
                  src="https://pyxis.nymag.com/v1/imgs/121/c34/e4c5baee3baf2daf08bd1a19c7ad98aa48-31-nick-viall.rsquare.w700.jpg"
                  className="user-image rounded-circle"
                  alt="image"
                />
                <div className="px-3">
                  <h4 className="mb-1">{data.firstName}</h4>
                  <p>{data.address}</p>
                </div>
              </div>
            </div>
            <div className="col-md-6 d-flex justify-content-center align-items-center">
              <div>
                <button className="btn btn-dark">
                  <Link to="/cart" className="text-decoration-none text-white">
                    Xem giỏ hàng
                  </Link>
                </button>
              </div>
            </div>
          </div>

          <div className="row mx-5 py-4">
            <div className="col-md-6 border-right">
              <h5>Chi tiết tài khoản</h5>
              <div className="py-3" id={styles.userInfo}>
                <div className="d-flex justify-content-between">
                  <div className={styles.width50}>
                    <p className="m-0">HỌ TÊN</p>
                    <p>{data.firstName}</p>
                  </div>

                  <div className={styles.width50}>
                    <p className="m-0">ĐỊA CHỈ</p>
                    <p>{data.address}</p>
                  </div>
                </div>

                <div className="d-flex justify-content-between">
                  <div className={styles.width50}>
                    <p className="m-0">EMAIL</p>
                    <p>{data.email}</p>
                  </div>
                  <div className={styles.width50}>
                    <p className="m-0">ĐIỆN THOẠI</p>
                    <p>{data.phone}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <h5>Địa chỉ nhận hàng</h5>
              <div className="py-3" id={styles.userBilling}>
                <div className="d-flex justify-content-between">
                  <div className={styles.width50}>
                    <p className="m-0">HỌ TÊN</p>
                    <p>{data.firstName}</p>
                  </div>
                  <div className={styles.width50}>
                    <p className="m-0">ĐỊA CHỈ</p>
                    <p>{data.address}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default UserAccount;
