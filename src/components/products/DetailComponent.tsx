import React, { useEffect, useState } from "react";
import { CartPlusFill, Heart } from "react-bootstrap-icons";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import QuantitySelect from "../../common-ui/QuantitySelect";
import styles from "./DetailComponent.module.css";
import { getDataById } from "../../store/product/action-creators";
import AddToCartButton from "../../common-ui/AddToCartButton";
import Optional from "./Optional";
import AddToFavourite from "../../common-ui/AddToFavourite";
import LoadingComponent from "../../common-ui/LoadingComponent";

function DetailComponent() {
  let { id } = useParams<any>();
  const dispatch = useDispatch();
  const [quan, setQuan] = useState<number>(1);
  const [color, setColor] = useState<string>("");
  const [size, setSize] = useState<string>("");
  const state = useSelector((state: any) => state.productData);
  const loadingAdFavorites = useSelector(
    (state: any) => state.favoritesData.loading
  );
  const { productId } = state;
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  const onIncrease = () => {
    setQuan(quan + 1);
  };

  const onDecrease = () => {
    if (quan === 1) {
      return;
    }
    setQuan(quan - 1);
  };

  const chooseColor = (e: any) => {
    setColor(e.target.value);
  };

  const chooseSize = (e: any) => {
    setSize(e.target.value);
  };

  return (
    <React.Fragment>
      <section className={`container ${styles.minHeight}`}>
        <div className="row">
          <div className="col-md-6 pl-0 pr-md-5">
            <img
              className="w-100 rounded"
              src={productId.imageDefault}
              alt=""
            />
          </div>
          <div className="col-md-6">
            <h3>{productId.name}</h3>
            <p className="border-bottom pb-4">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Temporibus magni assumenda omnis nostrum vitae? Mollitia, dolores
              aperiam ipsa sunt in dolorum perspiciatis labore similique non ab.
              Perferendis quos fuga aperiam.
            </p>
            <div className="bottom">
              {productId.size?.length > 0 && (
                <Optional
                  data={productId.size}
                  onSelect={chooseSize}
                  name="Kích cỡ"
                  state={size}
                />
              )}
              {productId.color?.length > 0 && (
                <Optional
                  data={productId.color}
                  onSelect={chooseColor}
                  name="Màu sắc"
                  state={color}
                />
              )}

              <div className="quantity row align-items-center mt-4">
                <p className="mb-1 col-md-3 h6">Số lượng:</p>
                <div className="left col-md-6">
                  <QuantitySelect
                    quantity={quan}
                    onIncrease={onIncrease}
                    onDecrease={onDecrease}
                  />
                </div>
              </div>
              <div className="total row align-items-center mt-4">
                <p className="mb-0 col-md-3 h6">Tổng:</p>
                <div className="left col-md-6">{productId.price * quan} đ</div>
              </div>
              <div className="button-group mt-4">
                <AddToCartButton
                  name={productId.name}
                  price={productId.price}
                  color={color}
                  imageDefault={productId.imageDefault}
                  id={productId.id}
                  quantity={quan}
                  buttonColor="success"
                  className="mr-3"
                  content={
                    <span className="d-flex align-items-center">
                      <CartPlusFill size={20} className="mr-2" />
                      Thêm giỏ hàng
                    </span>
                  }
                />
                <AddToFavourite
                  name={productId.name}
                  price={productId.price}
                  color={color}
                  imageDefault={productId.imageDefault}
                  id={productId.id}
                  buttonColor="danger"
                  quantity={quan}
                  size={productId.size}
                  className={`${loadingAdFavorites && "disabled"}`}
                  content={
                    loadingAdFavorites ? (
                      <LoadingComponent size="sm" />
                    ) : (
                      <Heart size={20} color="white" className="iconHover" />
                    )
                  }
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
}
export default DetailComponent;
