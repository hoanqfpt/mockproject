import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";

import {sortData} from '../../store/product/action-creators'
function HeaderProduct() {
  const [value, setValue] = useState("sort");
const dispatch = useDispatch();

  const onChange = (event: any) => {
    // console.log(event.target.value);
    setValue(event.target.value);
    const action = sortData(event.target.value);
    dispatch(action);
  };
  return (
    <React.Fragment>
      <div className="d-flex justify-content-between mb-2 mt-2">
        <p className="">
          {/* <Link to="/">Home</Link> / Products */}
        </p>
        {/* <form>
          <select className="form-control" onChange={onChange} value={value}>
            <option value="sort">Sort</option>
            <option value="new">Sản phẩm mới nhất</option>
            <option value="minmax">Giá rẻ nhất</option>
            <option value="maxmin">Giá đắt nhất</option>
            <option value="hot">Bán chạy nhất</option>
          </select>
        </form> */}
      </div>
    </React.Fragment>
  );
}
export default HeaderProduct;
