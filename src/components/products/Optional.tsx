import React from "react";
import styles from "./DetailComponent.module.css";

interface IOptionalProps {
  data: string[];
  name: string;
  state: string;
  onSelect: (e: any) => void;
}

const Optional = (Props: IOptionalProps) => {
  const { data, state, onSelect, name } = Props;
  return (
    <div className="size row align-items-center mt-4">
      <p className="mb-0 col-md-3 h6 mb-1">{name}:</p>
      <div className="left col-md-6">
        {data?.map((v: string) => {
          return (
            <div key={v} className="d-inline-block">
              <input
                type="radio"
                id={v}
                name="size"
                className="d-none"
                value={v}
                onClick={onSelect}
              />
              <label
                className={`${styles.information} ${
                  state == v ? styles.selected : styles.notSelect
                }`}
                style={{ background: `${v}` }}
                htmlFor={v}
              >
                <span className="user-select-none">
                  {name === "Màu sắc" ? "" : v}
                </span>
              </label>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Optional;
