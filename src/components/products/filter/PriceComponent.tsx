import React, { useState } from "react";
import { Slider } from "antd";
import DividerComponent from "../../../common-ui/DividerComponent";
import {getDataByPrice } from '../../../store/product/action-creators';
import {useDispatch, useSelector} from "react-redux"

function PriceComponent() {
  const [minPrice, setMinPrice] = useState("0");
  const [maxPrice, setMaxPrice] = useState("2000000");
  const dispatch = useDispatch();

  const onChange = (value: any) => {
    
    const minPrice = new Intl.NumberFormat('vn-VN', { maximumSignificantDigits: 3 }).format(value[0]);
    const maxPrice = new Intl.NumberFormat('vn-VN', { maximumSignificantDigits: 3 }).format(value[1]);
    setMinPrice(minPrice);
    setMaxPrice(maxPrice);
  };
  const isFilter = useSelector((state: any) => state.productData.isFilterPrice);
  const onAfterChange = (value: any) => {
    const action = getDataByPrice(value);
    dispatch(action);
  };
  return (
    <React.Fragment>
      <DividerComponent name="Giá" align="center" />
      <Slider
        range
        step={10}
        max={2000000}
        defaultValue={isFilter || [0, 2000000]}
        onChange={onChange}
        onAfterChange={onAfterChange}
      />

      <form>
        <div className="form-group row">
          <label htmlFor="fromPrice" className="col-sm-4 col-form-label">
            Giá từ:
          </label>
          <div className="col-sm-8">
            <input
              value={!isFilter ? 0 : minPrice}
              type="text"
              className="form-control"
              id="fromPrice"
              disabled
            />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="toPrice" className="col-sm-4 col-form-label">
            Giá đến:
          </label>
          <div className="col-sm-8">
            <input
              value={maxPrice}
              type="text"
              className="form-control"
              id="toPrice"
              disabled
            />
          </div>
        </div>
      </form>
    </React.Fragment>
  );
}
export default PriceComponent;
