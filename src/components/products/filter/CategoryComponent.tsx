import React, { useEffect, useState } from "react";
import DividerComponent from "../../../common-ui/DividerComponent";
import { useDispatch, useSelector } from "react-redux";
import {
  getCategories,
  getDataByCateID,
} from "../../../store/product/action-creators";

function CategoryComponent() {
  const [active, setActive] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    const action = getCategories();
    dispatch(action);
  }, []);
  const data = useSelector((state: any) => state.productData);
  const { isFilterCate, categories } = data;
  const onClickCate = (id: string) => {
    setActive(id);
    const action = getDataByCateID(id);
    dispatch(action);
  };
  return (
    <React.Fragment>
      <DividerComponent name="Loại giày" align="center" />
      <ul className="list-group">
        {categories &&
          categories.map((item: any) => {
            return (
              <li
                onClick={() => onClickCate(item.id)}
                key={item.id}
                style={{ cursor: "pointer" }}
                className={`list-group-item  ${
                  isFilterCate ? (item.id === active ? "active" : "") : ""
                } `}
              >
                {item.name}
              </li>
            );
          })}
      </ul>
    </React.Fragment>
  );
}
export default CategoryComponent;
