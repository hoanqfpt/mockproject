import React, { useState } from "react";
import DividerComponent from "../../../common-ui/DividerComponent";
import ButtonComponent from "../../../common-ui/ButtonComponent";
import { useDispatch ,useSelector} from "react-redux";
import {getDataBySize,getDataBySizeRedux} from '../../../store/product/action-creators'
const SIZE = [
  { name: "35" },
  { name: "36" },
  { name: "37" },
  { name: "38" },
  { name: "39" },
  { name: "40" },
  { name: "41" },
  { name: "42" },
  { name: "43" },
];

function SizeComponent() {
  const data = useSelector((state:any)=> state.productData);
  const {isFilterSize,isFilterCate} = data;
  const [active, setActive] = useState("");
  const dispatch =useDispatch();
  
  const onClickChangeSize = (item: string) => {
    setActive(item);
    // if(!isFilterCate)
    // { 
    //   const action = getDataBySize(item);
    //   dispatch(action);
    // }
    // else{
      const action = getDataBySizeRedux(item);
      dispatch(action);
    // }
  };
  return (
    <React.Fragment>
      <DividerComponent name="Size" align="center" />
      {SIZE.map((item, i) => {
        return (
          <ButtonComponent
            name={item.name}
            color="danger"
            outline
            key={i}
            className={`w-25 mr-3 mt-1 ${isFilterSize?active === item.name ? "active" : "":""}`}
            onClick={() => onClickChangeSize(item.name)}
          />
        );
      })}
    </React.Fragment>
  );
}
export default SizeComponent;
