import React from "react";
import { CartPlus, Heart } from "react-bootstrap-icons";
import { useHistory } from "react-router-dom";
import { addToCart, addToCartAPI } from "../../store/cart/action-creators";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { addFavorites } from "../../store/favorites/action-creators";
import LoadingComponent from "../../common-ui/LoadingComponent";
import { formatNumber } from "../../util/common";

interface IBoxComponent {
  name: string;
  price: number;
  color: string[];
  size: string[];
  imageDefault: string;
  id: string;
  categoryName: string;
}
toast.configure();
function BoxProductComponent(Props: IBoxComponent) {
  const { name, price, color, imageDefault, size, id, categoryName } = Props;
  const dispatch = useDispatch();
  const history = useHistory();
  const loadingAdFavorites = useSelector(
    (state: any) => state.favoritesData.loading
  );
  const loadingAddcart = useSelector((state: any) => state.cartData.loading);
  
  const onClickDetail = () => {
    history.push("/products/" + id);
  };
  const cartItem: any = {
    name: name,
    price: price,
    color: color,
    imageDefault: imageDefault,
    maSP: id,
    quantity: 1,
  };
  const onClickAddToCart = () => {
    const localCart: string | null = localStorage.getItem("cartItem");
    const listCart: string[] = JSON.parse(
      localCart !== null ? localCart : "[]"
    );
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      dispatch(addToCartAPI(cartItem));
      return;
    }
    const check = listCart.filter((item: any) => {
      return item.maSP === id;
    });
    if (check.length > 0) {
      toast.warn("Sản phẩm đã có trong giỏ hàng", {
        autoClose: 1500,
        className: "mt-5",
      });
      return;
    }
    listCart.push(cartItem);
    dispatch(addToCart(listCart));
    localStorage.setItem("cartItem", JSON.stringify(listCart));
    toast.success("Đã được thêm vào giỏ hàng", {
      autoClose: 1500,
      className: "mt-5",
    });
  };
  const onClickAddToFavorites = () => {
    const maUser = localStorage.getItem("idUser");
    if (maUser === null) {
      toast.warn("Vui lòng đăng nhập!", {
        autoClose: 1500,
        className: "mt-5",
      });
      return;
    }
    dispatch(
      addFavorites({
        nameProduct: name,
        price: price,
        color: color,
        imageDefault: imageDefault,
        maSP: id,
        desProduct: `${color.length} colour - ${size.length} size`,
        maUser: Number(maUser),
        quantity:1
      })
    );
  };
  return (
    <React.Fragment>
      <div className="position-relative   boxAddProductToBag">
        <div className="box-zoom-transfer">
          <img
            onClick={onClickDetail}
            src={
              imageDefault !== ""
                ? imageDefault
                : "https://www.chanchao.com.tw/VietnamPrintPack/images/default.jpg"
            }
            className="card-img-top w-100"
            alt="..."
          />
        </div>
        <div className="card-body  pl-0">
          <h5 onClick={onClickDetail} className="card-title">
            {name}
          </h5>
          <p className="card-text m-0 font-italic">{categoryName}</p>
          <p className="card-text m-0 font-italic">
            {color.length} colour - {size.length} size
          </p>
          <p className="card-text m-0 font-weight-normal">
            {formatNumber(price)} vnđ
          </p>
          <div className="row justify-content-end position-absolute addProductToBag">
            <button
              className={`btn btn-primary shadow-sm  ${
                loadingAddcart && "disabled"
              }`}
              onClick={onClickAddToCart}
            >
              {loadingAddcart && "disabled" ? (
                <LoadingComponent size="sm" />
              ) : (
                <CartPlus size={20} className="iconHover" color="white" />
              )}
            </button>
            <button
              className={`btn btn-danger ml-1 ${
                loadingAdFavorites && "disabled"
              }`}
              onClick={onClickAddToFavorites}
            >
              {loadingAdFavorites ? (
                <LoadingComponent size="sm" />
              ) : (
                <Heart size={20} color="white" className="iconHover" />
              )}
            </button>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default BoxProductComponent;
