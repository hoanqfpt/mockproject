import {
  Formik,
  Field,
  Form,
  FormikProps,
  FormikHelpers,
} from "formik";
import InputField from "../../common-ui/InputField";
import * as Yup from "yup";
import { Container, FormGroup, Row } from "reactstrap";
import {useParams, Link,useHistory } from "react-router-dom";
import {confirmPassword} from '../../store/user/action-creators'
import { useDispatch } from "react-redux";

export interface IFormValue {
  password: string;
  isShowPass: boolean;
}

function ConfirmPassword() {
  const dispatch = useDispatch();
  const history = useHistory();
  const initialValues: IFormValue = {
    password: "",
    isShowPass: false,
  };
  const param:any = useParams();
  const validationSchema = Yup.object().shape({
    password: Yup.string().required("Mật khẩu không để trống"),
  });

  return (
    <Container>
      <h5 className="text-center mt-5 mb-5">Nhập mật khẩu mới</h5>
      <Row className="d-flex justify-content-center align-items-center" xs="3">
        <Formik
          initialValues={initialValues}
          onSubmit={(values, formikHelpers: FormikHelpers<IFormValue>) => {
            dispatch(confirmPassword(param.forgotToken, values, history));
          }}
          validationSchema={validationSchema}
        >
          {(formikProps: FormikProps<IFormValue>) => {
            return (
              <Form>
                <Field
                  name="password"
                  component={InputField}
                  label="Mật khẩu"
                  placeholder="Nhập mật khẩu reset"
                  type={formikProps.values.isShowPass ? "text" : "password"}
                />
                <FormGroup>
                  <label>
                    <Field type="checkbox" name="isShowPass" />
                      Hiển thị mật khẩu
                  </label>
                </FormGroup>
                <button
                  type="submit"
                  className="btn btn-info btn-block rounded-pill"
                  disabled={!formikProps.isValid || !formikProps.dirty}
                >
                  RESET PASSWORD
                </button>

                <FormGroup className="mt-3 text-center">
                  <label htmlFor="">Bạn đã nhớ mật khẩu?</label>
                  <Link to='/login' className="text-decoration-none text-info ml-1">
                    Đăng nhập
                  </Link>
                </FormGroup>
              </Form>
            );
          }}
        </Formik>
      </Row>
    </Container>
  );
}

export default ConfirmPassword;
