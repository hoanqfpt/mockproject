import React, { useEffect } from "react";
import {
  Formik,
  Field,
  Form,
  FastField,
  FormikProps,
  FormikHelpers,
} from "formik";
import InputField from "../../common-ui/InputField";
import * as Yup from "yup";
import { Container, FormGroup, Row } from "reactstrap";
import { Link } from "react-router-dom";
import { signUp } from "../../store/user/action-creators";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { toast } from "react-toastify";

export interface IFormValue {
  firstName: string;
  phone: string;
  address: string;
  email: string;
  password: string;
  confirmPassword: string;
}

function SignUp() {
  const dispatch = useDispatch();
  const history = useHistory();
  const error = useSelector((state: any) => state.userData.error);
  useEffect(() => {
    if (error !== "") {
      toast.warn("Đăng ký thất bại", {
        autoClose: 2000,
        className: "mt-5",
      });
    }
  }, [error]);
  const initialValues: IFormValue = {
    firstName: "",
    phone: "",
    address: "",
    email: "",
    password: "",
    confirmPassword: "",
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Họ tên không để trống"),
    email: Yup.string()
      .email("Email không đúng định dạng")
      .required("Email không để trống"),
    password: Yup.string().min(2).required("Password không để trống"),
    confirmPassword: Yup.string()
      // .required("")
      .oneOf([Yup.ref("password"), null], "Mật khẩu không khớp!"),
  });

  return (
    <Container>
      <h1 className="text-center mt-5 mb-4">SignUp</h1>
      <Row className="d-flex justify-content-center align-items-center" xs="3">
        <Formik
          initialValues={initialValues}
          onSubmit={(values, formikHelpers: FormikHelpers<IFormValue>) => {
            dispatch(signUp(values, history));
            //formikHelpers.resetForm();
          }}
          validationSchema={validationSchema}
        >
          {(formikProps: FormikProps<IFormValue>) => {
            return (
              <Form>
                <FastField
                  name="firstName"
                  component={InputField}
                  label="Họ tên"
                  placeholder="Nhập họ tên"
                />
                <FastField
                  name="email"
                  component={InputField}
                  label="Email"
                  placeholder="Nhập email"
                />
                <FastField
                  name="phone"
                  component={InputField}
                  label="Điện thoại"
                  placeholder="Nhập điện thoại"
                />
                <FastField
                  name="address"
                  component={InputField}
                  label="Địa chỉ"
                  placeholder="Nhập địa chỉ"
                />
                <Field
                  name="password"
                  component={InputField}
                  placeholder=""
                  label="Password"
                  type="password"
                />
                <Field
                  name="confirmPassword"
                  component={InputField}
                  placeholder=""
                  label="Nhập lại mật khẩu"
                  type="password"
                />
                {error && <p className="text-danger text-center">{error}</p>}

                <button
                  type="submit"
                  className="btn btn-info btn-block rounded-pill"
                  disabled={!formikProps.isValid || !formikProps.dirty}
                >
                  RESIGTER
                </button>
                <FormGroup className="mt-3 text-center">
                  <Link to="/login" className="text-decoration-none text-info ">
                    Already have an account?
                  </Link>
                </FormGroup>
              </Form>
            );
          }}
        </Formik>
      </Row>
    </Container>
  );
}

export default SignUp;
