import React from "react";
import {
  Formik,
  Field,
  Form,
  FastField,
  FormikProps,
  FormikHelpers,
} from "formik";
import InputField from "../../common-ui/InputField";
import * as Yup from "yup";
import { Container, FormGroup, Row } from "reactstrap";
import { Link } from "react-router-dom";
import {resetPassword} from '../../store/user/action-creators';
import { useDispatch,useSelector } from "react-redux";

export interface IFormValue {
  email: string;
}

function ResetPassword() {
  const dispatch =useDispatch();
  const loading = useSelector((state: any) => state.userData.loading);
  const isDone = useSelector((state: any) => state.userData.isDoneResetPass);
  const initialValues: IFormValue = {
    email: "",
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().email("Email không đúng định dạng").required("Email không được bỏ trống"),
  });

  return (
    <Container>
      {isDone ? (
        <h5 className="text-center text-success mt-5 mb-5">Vui lòng kiểm tra email của bạn! Xin cảm ơn!</h5>
      ) : (
        <React.Fragment>
          <h5 className="text-center mt-5 mb-5">Lấy lại mật khẩu</h5>
          <Row
            className="d-flex justify-content-center align-items-center"
            xs="3"
          >
            <Formik
              initialValues={initialValues}
              onSubmit={(values, formikHelpers: FormikHelpers<IFormValue>) => {
                dispatch(resetPassword(values));
              }}
              validationSchema={validationSchema}
            >
              {(formikProps: FormikProps<IFormValue>) => {
                return (
                  <Form>
                    <FastField
                      name="email"
                      component={InputField}
                      placeholder="Nhập email"
                    />
                    <button
                      type="submit"
                      className="btn btn-info btn-block rounded-pill"
                      disabled={
                        loading
                          ? true
                          : !formikProps.isValid || !formikProps.dirty
                      }
                    >
                      {loading ? (
                        <div
                          className="spinner-border  text-warning"
                          role="status"
                        ></div>
                      ) : (
                        " RESET MẬT KHẨU"
                      )}
                    </button>

                    <FormGroup className="mt-3 text-center">
                      <label htmlFor="">Bạn đã nhớ mật khẩu?</label>
                      <Link
                        to="/login"
                        className="text-decoration-none text-info ml-1"
                      >
                        Đăng nhập
                      </Link>
                    </FormGroup>
                  </Form>
                );
              }}
            </Formik>
          </Row>
        </React.Fragment>
      )}
    </Container>
  );
}

export default ResetPassword;
