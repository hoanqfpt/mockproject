import React from "react";
import SignIn from "./SignIn";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function AuthenContainer() {
  return <SignIn />;
}

export default AuthenContainer;
