import React from "react";
import {
  Formik,
  Field,
  Form,
  FastField,
  FormikProps,
  FormikHelpers,
} from "formik";
import InputField from "../../common-ui/InputField";
import * as Yup from "yup";
import { Container, FormGroup, Row } from "reactstrap";
import { login } from "../../store/user/action-creators";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";

export interface IFormValue {
  email: string;
  password: string;
  isShowPass: boolean;
}

function SignIn() {
  const dispatch = useDispatch();
  const history = useHistory();
  const loading = useSelector((state: any) => state.userData.loading);
  const loginError = useSelector((state: any) => state.userData.loginError);
  const initialValues: IFormValue = {
    email: "",
    password: "",
    isShowPass: false,
  };
  const validationSchema = Yup.object().shape({
    email: Yup.string().email("Email invalid").required("Email required"),
    password: Yup.string().required("Password required"),
  });

  return (
    <Container className="mb-5">
      <h1 className="text-center mt-5 mb-5">Login</h1>
      {loginError && <p className="text-center text-danger">{loginError}</p>}

      <Row className="d-flex justify-content-center align-items-center" xs="3">
        <Formik
          initialValues={initialValues}
          onSubmit={(values, formikHelpers: FormikHelpers<IFormValue>) => {
            dispatch(login(values, history));
            //formikHelpers.resetForm();
          }}
          validationSchema={validationSchema}
        >
          {(formikProps: FormikProps<IFormValue>) => {
            return (
              <Form>
                <FastField
                  name="email"
                  component={InputField}
                  placeholder="Nhập email"
                />
                <Field
                  name="password"
                  component={InputField}
                  placeholder="Nhập mật khẩu"
                  type={formikProps.values.isShowPass ? "text" : "password"}
                />
                <FormGroup className="d-flex justify-content-between">
                  <label>
                    <Field type="checkbox" name="isShowPass" />
                    Hiển thị mật khẩu
                  </label>
                  <Link to='/reset-password' className="text-decoration-none text-info">
                    Quên mật khẩu?
                  </Link>
                </FormGroup>

                <button
                  type="submit"
                  className="btn btn-info btn-block rounded-pill"
                  disabled={
                    loading ? true : !formikProps.isValid || !formikProps.dirty
                  }
                >
                  {loading ? (
                    <div
                      className="spinner-border  text-warning"
                      role="status"
                    ></div>
                  ) : (
                    "LOGIN"
                  )}
                </button>

                <FormGroup className="mt-3 text-center">
                  <label>Bạn chưa có tài khoản?</label>
                  <Link
                    to="/signup"
                    className="text-decoration-none text-info ml-1"
                  >
                    Đăng ký
                  </Link>
                </FormGroup>
              </Form>
            );
          }}
        </Formik>
      </Row>
    </Container>
  );
}

export default SignIn;
