import React, { useEffect, useState } from "react";
import { BagCheck, BoxArrowRight, Heart, Person, PersonCircle } from "react-bootstrap-icons";
import { NavLink, useHistory } from "react-router-dom";
import { Button, Dropdown } from "antd";
import CartDropdown from "./cart/CartDropdown";
import { useSelector,useDispatch } from "react-redux";
import DividerComponent from "../common-ui/DividerComponent";
import {logoutUser} from '../store/user/action-creators'

import { toast } from "react-toastify";
import { getCartAPI, getCartStorages } from "../store/cart/action-creators";

function Header() {
  const [valueSearch, setValueSearch] = useState("");
  
  const dataCart = useSelector((state: any) => state.cartData.carts);
  const localCart: string | null = localStorage.getItem("cartItem");
  const listCart: string[] = JSON.parse(localCart !== null ? localCart : "[]");

  const history = useHistory();
  const dispatch =useDispatch();
  const logOut = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("lastName");
    localStorage.removeItem("idUser");
    dispatch(logoutUser());
    history.push("/");
    toast.success("Bạn đã đăng xuất!", { autoClose: 1500,className:"mt-5" });
  };
  const maUser = localStorage.getItem("idUser");
  useEffect(() => {
    
    if (maUser !== null) {
      dispatch(getCartAPI(maUser));
    } else {
      dispatch(getCartStorages(listCart));
    }
  },[])
  const onChangeValue = (e: any) => {
    setValueSearch(e.target.value);
  };
  const onKeyUp = (e: any) => {
    e.preventDefault();
    if (e.key === "Enter") {
      history.push("/search/" + valueSearch);
      setValueSearch("");
    }
  };
  const nameUser = localStorage.getItem("lastName");
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top shadow">
        <NavLink className="navbar-brand" to="/">
          B L U E C H I C
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav m-auto">
            <li className="nav-item">
              <NavLink to="/home" className="nav-link">
                Trang chủ
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/about" className="nav-link">
                Giới thiệu
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/products" className="nav-link">
                Sản phẩm
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/lookbook" className="nav-link">
                LookBook
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/contact" className="nav-link">
                Liên hệ
              </NavLink>
            </li>
            {nameUser && maUser === "1" && (
              <li className="nav-item">
                <NavLink to="/admin" className="nav-link">
                  Quản trị
                </NavLink>
              </li>
            )}
          </ul>
          <div className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              value={valueSearch}
              onChange={(e) => onChangeValue(e)}
              onKeyUp={(e) => onKeyUp(e)}
            />
          </div>
          <div className="d-flex">
            {nameUser === null ? (
              <NavLink to="/login" className="nav-link p-0">
                <PersonCircle className="info mr-3 iconHover" size={22} />
              </NavLink>
            ) : (
              <React.Fragment>
                <NavLink to="/account" className="nav-link p-0 mr-3">
                  <PersonCircle
                    className="info mr-1 iconHover"
                    color="#528078"
                    size={22}
                  />
                  {nameUser}
                </NavLink>

                <a
                  href="javascript:void(0)"
                  className="nav-link p-0"
                  onClick={logOut}
                >
                  <BoxArrowRight
                    className="info iconHover ml-0 pl-0"
                    size={25}
                    color="#528078"
                  />
                  Logout
                </a>
                <DividerComponent type={"vertical"} />
              </React.Fragment>
            )}

            <Dropdown
              overlay={dataCart && <CartDropdown data={dataCart} />}
              overlayClassName="shadow"
            >
              <BagCheck
                className="info iconHover"
                color="#528078"
                size={22}
                style={{ cursor: "pointer" }}
              />
            </Dropdown>
            <span className="text-info ">
              ({dataCart.length > 0 ? dataCart.length : listCart.length})
            </span>
            <NavLink to="/favorites" className="nav-link p-0">
              <Heart className="info ml-3 iconHover" size={22} />
            </NavLink>
          </div>
        </div>
      </nav>
    </React.Fragment>
  );
}

export default Header;
