import style from "../about/ReasonComponent.module.css";

interface IReasonComponentProps {
  title: string;
  content: string;
  img: string;
  id: number;
}

const ReasonComponent = (Props: IReasonComponentProps) => {
  const { title, content, img, id } = Props;
  return (
    <div
      className={`${style.reasonItem} row d-flex justify-content-center align-items-center`}
    >
      <div
        className={`col-md-5 col-sm-12 px-5 ${
          id % 2 === 0 ? "order-2" : "order-1"
        }`}
      >
        <h3 className="mb-3">{title}</h3>
        <p>{content}</p>
      </div>
      <div
        className={`col-md-5 col-sm-12 px-5 ${
          id % 2 === 0 ? "order-1" : "order-2"
        }`}
      >
        <img src={img} className="w-100" alt={title} />
      </div>
    </div>
  );
};

export default ReasonComponent;
