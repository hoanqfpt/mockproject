import style from "../about/AboutHeader.module.css";

interface IAboutHeaderProps {
  title: string;
}

const AboutHeader = (Props: IAboutHeaderProps) => {
  return (
    <section className="header">
      <div className={style.headerWrapper}>
        <h1>{Props.title}</h1>
      </div>
    </section>
  );
};

export default AboutHeader;
