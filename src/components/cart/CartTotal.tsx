import { Menu } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
import style from "./CartDropdownItem.module.css";

interface ICartTotalrops {
  data: string[];
}

const CartTotal = (Props: ICartTotalrops) => {
  const { data } = Props;
  const sum = () => {
    const total = data.reduce((total: any, item: any) => {
      return total + item.price;
    }, 0);
    return total;
  };
  return (
    <div className="text-center text-uppercase py-3">
      <h6 className="mb-3 px-3">
        {data.length > 0 ? `Tổng tiền: ${sum()} vnđ` : `Giỏ hàng đang trống`}
      </h6>
      <Menu.Item className={`${style.hoverCard}`} key="total">
        <NavLink
          to="/cart"
          className="d-block text-uppercase text-decoration-none bg-dark text-white"
        >
          <p className="font-weight-bold m-0">Xem giỏ hàng</p>
        </NavLink>
      </Menu.Item>
    </div>
  );
};

export default CartTotal;
