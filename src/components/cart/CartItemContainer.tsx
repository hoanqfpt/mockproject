import React from "react";
import { XCircle } from "react-bootstrap-icons";
import CartDropdownItem from "./CartDropdownItem";

interface ICartItemContainerProps {
  data: string[];
}

const CartItemContainer = (Props: ICartItemContainerProps) => {
  const { data } = Props;
  return (
    <div style={{ maxHeight: "300px", overflow: "auto" }}>
      {data &&
        data.map((item: any) => {
          return (
            <CartDropdownItem
              key={item.maSP}
              name={item.name}
              price={item.price}
              id={item.maSP}
              close={<XCircle />}
              image={item.imageDefault}
              maSP={item.maSP}
            />
          );
        })}
    </div>
  );
};

export default CartItemContainer;
