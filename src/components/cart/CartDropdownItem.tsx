import React from "react";
import { NavLink } from "react-router-dom";
import { Menu } from "antd";
import style from "./CartDropdownItem.module.css";
import {useDispatch} from 'react-redux'
import {addToCart, deleteCartAPI} from '../../store/cart/action-creators'
interface ICartDropdownItemProps {
  name: string;
  price: string;
  close: React.ReactElement;
  image: string;
  id:string;
  maSP:string;
}

const CartDropdownItem = (Props: ICartDropdownItemProps) => {
  const { name, price, close, image, id, maSP } = Props;
  const dispatch = useDispatch();

  const onClickDelete = ()=>{
    const localCart: string | null = localStorage.getItem("cartItem");
    const listCart:string[] =  JSON.parse(localCart !== null ? localCart : "[]");
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      dispatch(deleteCartAPI(maSP));
    } else {
      const listCartDelete =  listCart.filter((item:any)=>{ return item.maSP !== id});
      dispatch(addToCart(listCartDelete));
      localStorage.setItem("cartItem", JSON.stringify(listCartDelete));
    }
  }
  return (
    <div className="d-flex align-items-center border-bottom">
      <Menu.Item className={`px-4 py-3 ${style.hoverCard}`} key={maSP} style={{minHeight: "100px"}}>
        <NavLink
          to={`/products/${maSP}`}
          className="d-flex align-items-center text-decoration-none"
        >
          <img src={image} className={style.cartDropdownImg} />
          <div className="ml-4">
            <h6>{name}</h6>
            <p>{price}</p>
          </div>
        </NavLink>
      </Menu.Item>
      <div className="mx-3">
        <button className="btn btn-primary text-white" onClick={onClickDelete}>X</button>
      </div>
    </div>
  );
};

export default CartDropdownItem;
