import React from "react";
import style from "./CartItem.module.css";
import {
  addToCart,
  updateQuantityCartAPI,
  deleteCartAPI,
} from "../../store/cart/action-creators";
import { TrashFill, Heart } from "react-bootstrap-icons";
import { useDispatch, useSelector } from "react-redux";
import ButtonComponent from "../../common-ui/ButtonComponent";
import QuantitySelect from "../../common-ui/QuantitySelect";
import { toast } from "react-toastify";
import { addFavorites } from "../../store/favorites/action-creators";
import LoadingComponent from "../../common-ui/LoadingComponent";

interface ICartItemProps {
  imgProduct: string;
  nameProduct: string;
  price: number;
  index: number;
  maSP: string;
  quantity: number;
  color: string[];
  size: number[];
}

export interface CART {
  name: string;
  color: string[];
  id: string;
  price: number;
  imageDefault: string;
  quantity: number;
}

const CartItem = (Props: ICartItemProps) => {
  const { imgProduct, nameProduct, price, index, maSP, quantity, color, size } =
    Props;
  const dispatch = useDispatch();
  const localCart: string | null = localStorage.getItem("cartItem");
  const listCart: CART[] = JSON.parse(localCart !== null ? localCart : "[]");
  const loadingAdFavorites = useSelector(
    (state: any) => state.favoritesData.loading
  );
  const onDelete = () => {
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      dispatch(deleteCartAPI(maSP));
    } else {
      listCart.splice(index, 1);
      dispatch(addToCart(listCart));
      localStorage.setItem("cartItem", JSON.stringify(listCart));
    }
  };

  const onIncrease = () => {
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      dispatch(updateQuantityCartAPI({ maSP: maSP, quantity: quantity + 1 }));
    } else {
      listCart[index].quantity++;
      dispatch(addToCart(listCart));
      localStorage.setItem("cartItem", JSON.stringify(listCart));
    }
  };

  const onDecrease = () => {
     if (quantity === 1) {
       return;
     }
     const maUser = localStorage.getItem("idUser");
     if (maUser !== null) {
       dispatch(updateQuantityCartAPI({ maSP: maSP, quantity: quantity - 1 }));
     } else {
       listCart[index].quantity--;
       dispatch(addToCart(listCart));
       localStorage.setItem("cartItem", JSON.stringify(listCart));
     }
  };

  const addToFavorites = ()=>{
    const maUser = localStorage.getItem("idUser");
    if (maUser === null) {
      toast.warn("Vui lòng đăng nhập!", {
        autoClose: 1500,
        className: "mt-5",
      });
      return;
    }
    dispatch(
      addFavorites({
        nameProduct,
        price: price,
        color: color,
        imageDefault: imgProduct,
        maSP: maSP,
        desProduct: `1 colour - 1 size`,
        maUser: Number(maUser),
        quantity:1,
      })
    );
  }
  return (
    <>
      <tr className="border-bottom">
        <th>
          <img className={style.imgSize} src={imgProduct} alt={nameProduct} />
        </th>
        <th className="align-middle">{nameProduct}</th>
        <td className="text-center align-middle">{price}</td>
        <td className="text-center align-middle">
          <QuantitySelect
            quantity={quantity}
            onDecrease={onDecrease}
            onIncrease={onIncrease}
          />
        </td>
        <td className="text-center align-middle">{price * quantity}</td>
        <td className="text-center align-middle">
          <ButtonComponent
            name={<TrashFill color="white" />}
            className="btn btn-dark mx-2"
            onClick={onDelete}
          />
          <ButtonComponent
            name={
                loadingAdFavorites ? (
                <LoadingComponent size="sm" />
              ) : (
                <Heart size={20} color="white" className="iconHover" />
              )
            // <Heart color="white" />
          }
            className="btn btn-dark"
            onClick={addToFavorites}
          />
        </td>
      </tr>
    </>
  );
};

export default CartItem;
