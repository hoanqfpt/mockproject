import React from "react";
import CartDropdownItem from "./CartDropdownItem";
import { NavLink } from "react-router-dom";
import { Menu } from "antd";
import { XCircle } from "react-bootstrap-icons";
import { useSelector } from "react-redux";
import CartItemContainer from "./CartItemContainer";
import CartTotal from "./CartTotal";

interface ICartDropdownProps {
  data: string[];
}

const CartDropdown = (Props: ICartDropdownProps) => {
  const { data } = Props;
  return (
    <Menu className="p-0 mt-3">
      <CartItemContainer data={data} />
      <CartTotal data={data} />
    </Menu>
  );
};

export default CartDropdown;
