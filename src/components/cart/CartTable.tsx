import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import CartItem from "./CartItem";

const CartTable = () => {
  const dataCart = useSelector((state: any) => state.cartData.carts);
  return (
    <>
      <table className="table table-borderless">
        <thead className="thead-dark text-center">
          <tr>
            <th colSpan={2}>Sản phẩm</th>
            <th>Đơn giá</th>
            <th>Số lượng</th>
            <th>Tổng</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {dataCart?.map((v: any, i: number) => {
            return (
              <CartItem
                key={v.name}
                nameProduct={v.name}
                price={v.price}
                imgProduct={v.imageDefault}
                quantity={v.quantity}
                index={i}
                maSP={v.maSP}
                color={v.color}
                size = {v.size}
              />
            );
          })}
        </tbody>
      </table>
      <h5 className="text-right my-4">
        {dataCart?.reduce((total: number, v: any) => {
          return total + v.price * v.quantity;
        }, 0)}
      </h5>
    </>
  );
};

export default CartTable;
