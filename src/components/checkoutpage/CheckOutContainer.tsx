import React from "react";
import OrderDetails from "./checkoutcomponents/OrderDetails";
import OrderSummary from "./checkoutcomponents/OrderSummary";
import ShippingInfo from "./checkoutcomponents/ShippingInfo";
import { useSelector } from "react-redux";

function CheckOutContainer() {
  const dataCart = useSelector((state: any) => state.cartData.carts);

  return (
    <React.Fragment>
      <div className="check-out-wrapper" style={{ backgroundColor: "#f4f4f4" }}>
        <div className="container d-flex px-0 py-5 justify-content-between w-75">
          <div className="row w-100">
            <div className="col-md-7 p-0">
              <div className="order-info p-4 bg-light">
                <ShippingInfo />
              </div>
            </div>

            <div className="col-md-5 p-0">
              <div className="mx-4">
                <div>
                  <div className="order-summary p-3 bg-light">
                    <OrderSummary dataCart={dataCart} />
                  </div>
                  <div className="order-summary p-3 bg-light mt-3">
                    <OrderDetails dataCart={dataCart} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default CheckOutContainer;
