import React from "react";
import { CheckCircleOutlined } from "@ant-design/icons";
import styles from "./CheckOutSuccess.module.css";
import { Link } from "react-router-dom";
function CheckOutSuccess() {
  return (
    <React.Fragment>
      <div className={styles.container} style={{height:"50vh"}}>
        <CheckCircleOutlined className={styles.iconSuccess} />
        <h1>Thank You!</h1>
        <p>Quý khách đã mua hàng thành công.</p>
      </div>
    </React.Fragment>
  );
}

export default CheckOutSuccess;
