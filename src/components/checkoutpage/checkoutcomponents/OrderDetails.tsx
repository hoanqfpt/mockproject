import React from "react";

function OrderDetails(props: any) {
  const { dataCart } = props;
  return (
    <React.Fragment>
      <h3 style={{ color: "#333" }}>Chi tiết đơn hàng</h3>
      <div style={{ color: "#555" }}>
        <dl className="d-flex justify-content-between m-0">
          <dt>Số đơn hàng:</dt>
          <dd className="text-secondary small">#315526477</dd>
        </dl>

        <dl className="d-flex justify-content-between m-0">
          <dt>Phí ship:</dt>
          <dd className="text-secondary small">free</dd>
        </dl>

        <dl className="d-flex justify-content-between m-0">
          <dt>Số lượng:</dt>
          <dd className="text-secondary small">
            {dataCart?.reduce((total: number, v: any) => {
              return total + v.quantity;
            }, 0)}
          </dd>
        </dl>
      </div>

      <div>
        <dl className="d-flex justify-content-between m-0">
          <dt style={{ color: "#555" }}>Tổng:</dt>
          <dd className="text-secondary small">
            {dataCart?.reduce((total: number, v: any) => {
              return total + v.price * v.quantity;
            }, 0)}
          </dd>
        </dl>
      </div>
    </React.Fragment>
  );
}

export default OrderDetails;
