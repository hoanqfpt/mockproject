import React from "react";
import { CART } from "../../cart/CartItem";
function OrderSummary(props: any) {
  const { dataCart } = props;

  return (
    <React.Fragment>
      <h3 style={{ color: "#333" }}>Tóm tắt đơn hàng</h3>

      <div>
        <ul className="list-group list-unstyled">
          {dataCart.length > 0 &&
            dataCart.map((v: CART, i: number) => {
              return (
                <li key={i} className="mb-4">
                  <div>
                    <div className="d-flex justify-content-between">
                      <div>
                        <a href={v.imageDefault}>
                          <img
                            src={v.imageDefault}
                            alt="image"
                            style={{ width: "110px" }}
                          />
                        </a>
                      </div>
                      <div>
                        <p className="m-0">{v.name}</p>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between align-items-center mt-2">
                      <div
                        className="border"
                        style={{ width: "110px", height: "30px" }}
                      >
                        <div className="text-center text-secondary small pt-1">
                          Số lượng <span>{v.quantity}</span>
                        </div>
                      </div>

                      <p className="text-secondary small m-0">
                        Giá: {v.price * v.quantity}
                      </p>
                    </div>
                  </div>
                </li>
              );
            })}
        </ul>
      </div>
    </React.Fragment>
  );
}

export default OrderSummary;
