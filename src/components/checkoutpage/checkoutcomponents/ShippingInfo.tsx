import { useEffect } from "react";
import {
  Formik,
  Form,
  FormikProps,
  FormikHelpers,
} from "formik";
import CustomInputField from "../../../common-ui/CustomInputField";
import * as Yup from "yup";
import axios from "axios";
import { ORDER_API } from "../api";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { getUser } from "../../../store/user/action-creators";
import {
  getCartAPI,
  getCartStorages,
} from "../../../store/cart/action-creators";
import {
  deleteCartAPIByUser,
  deleteAllCartStorage,
} from "../../../store/cart/action-creators";

export interface IFormValue {
  username: string;
  email: string;
  phoneNumber: string;
  address: string;
}

function ShippingInfo() {
  const dataUser = useSelector((state: any) => state.userData.user);
  const dataCart = useSelector((state: any) => state.cartData.carts);

  const localCart: string | null = localStorage.getItem("cartItem");
  const listCart: any[] = JSON.parse(localCart !== null ? localCart : "[]");

  const idUser: string | null = localStorage.getItem("idUser");
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUser());
    if (idUser !== null) {
      dispatch(getCartAPI(idUser));
    } else {
      dispatch(getCartStorages(listCart));
    }
  }, []);
  const history = useHistory();

  const initialValues: IFormValue = {
    username: idUser ? dataUser?.firstName : "",
    email: idUser ? dataUser?.email : "",
    phoneNumber: idUser ? dataUser?.phone : "",
    address: idUser ? dataUser?.address : "",
  };

  const validationSchema = Yup.object().shape({
    username: Yup.string().required("Yêu cầu nhập tên khách hàng!"),
    email: Yup.string()
      .email("Email không hợp lệ!")
      .required("Yêu cầu nhập địa chỉ email!"),
    phoneNumber: Yup.string().required("Yêu cầu nhập số điện thoại!"),
    address: Yup.string().required("Yêu cầu nhập địa chỉ!"),
  });

  return (
    <div>
      <h3 style={{ color: "#333" }}>Thông tin khách hàng</h3>
      <div>
        <Formik
          enableReinitialize
          initialValues={initialValues}
          onSubmit={async (
            values,
            formikHelpers: FormikHelpers<IFormValue>
          ) => {
            const { username, email, phoneNumber, address } = values;
            try {
              const response = await axios.post(ORDER_API + "/order", {
                nameCustomer: username,
                address: address,
                idUser: idUser,
                email: email,
                phone: phoneNumber,
                dateCreate: Date.now(),
                total: dataCart?.reduce((total: number, v: any) => {
              return total + v.price * v.quantity;
            }, 0),
                products: dataCart.map((v: any) => {
                  return {
                    nameProduct: v.name,
                    price: v.price,
                    quantity: v.quantity,
                  };
                }),
              });
              if (response.status === 201) {
                 if (idUser !== null) {
                  dispatch(deleteCartAPIByUser(history));
                 } else {
                   localStorage.removeItem("cartItem");
                   history.push("/checkoutsuccess");
                   dispatch(deleteAllCartStorage());
                 }
                
              }
            } catch (error: any) {
              if (error) {
                alert(error);
              }
            }
          }}
          validationSchema={validationSchema}
        >
          {(formikProps: FormikProps<IFormValue>) => {
            const { values } = formikProps;
            return (
              <Form>
                <CustomInputField
                  name="username"
                  placeholder="Họ và Tên"
                  required
                  value={values.username}
                />
                <CustomInputField
                  name="email"
                  placeholder="Nhập Email"
                  required
                  value={values.email}
                />
                <CustomInputField
                  name="phoneNumber"
                  placeholder="Nhập điện thoại"
                  required
                  value={values.phoneNumber}
                />
                <CustomInputField
                  name="address"
                  placeholder="Nhập địa chỉ"
                  required
                  value={values.address}
                />
                <button
                  type="submit"
                  className="btn btn-dark btn-block rounded-pill text-white "
                >
                  ĐẶT HÀNG
                </button>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
}

export default ShippingInfo;
