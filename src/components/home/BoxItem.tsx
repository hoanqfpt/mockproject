import React from "react";
import ButtonComponent from "../../common-ui/ButtonComponent";
import style from "./BoxItem.module.css";
import { formatNumber } from "../../util/common";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { addToCart,addToCartAPI } from "../../store/cart/action-creators";
import { toast } from "react-toastify";
import LoadingComponent from "../../common-ui/LoadingComponent";

interface IBoxItemProps {
  name: string;
  imageDefault: string;
  price: number;
  color: string[];
  size: number[];
  oldPrice: number;
  id: string;
}

function BoxItem(Props: IBoxItemProps) {
      const loadingAddcart = useSelector(
        (state: any) => state.cartData.loading
      );
  const { name, imageDefault, price, color, size, oldPrice, id } = Props;
  const history = useHistory();
    const dispatch = useDispatch();
  const onClickDetail = () => {
    history.push("/products/" + id);
  };
  const onClickAddToCart = () => {  
      const cartItem: any = {
    name: name,
    price: price,
    color: color,
    imageDefault: imageDefault,
    maSP: id,
    quantity: 1,
  };
    const localCart: string | null = localStorage.getItem("cartItem");
    const listCart: string[] = JSON.parse(
      localCart !== null ? localCart : "[]"
    );
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      dispatch(addToCartAPI(cartItem));
      return;
    }
    const check = listCart.filter((item: any) => {
      return item.maSP === id;
    });
    if (check.length > 0) {
      toast.warn("Sản phẩm đã có trong giỏ hàng", {
        autoClose: 1500,
        className: "mt-5",
      });
      return;
    }
    listCart.push(cartItem);
    dispatch(addToCart(listCart));
    localStorage.setItem("cartItem", JSON.stringify(listCart));
    toast.success("Đã được thêm vào giỏ hàng", {
      autoClose: 1500,
      className: "mt-5",
    });
  };
  return (
    <React.Fragment>
      <div className={`col-md-3 ${style.itemHome}`}>
        <div className="">
          <div
            onClick={onClickDetail}
            className={style.productImg}
            style={{
              backgroundImage: `url(${imageDefault})`,
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              height: "280px",
            }}
          ></div>
        </div>
        <div className={`shadow m-auto rounded bg-light ${style.boxItemHome}`}>
          <div className="p-3">
            <h6>{name}</h6>
            <div>
              <span className="font-small text-muted">
                {color.length} colour - {size.length} size
              </span>
            </div>
            <div className="">
              <div className="mt-1 mb-3">
                <span className={`${style.newPrice} text-muted`}>
                  {formatNumber(price)} vnđ
                </span>
                {price < oldPrice && (
                  <span className={`${style.oldPrice}  text-danger`}>
                    {formatNumber(oldPrice)} vnđ
                  </span>
                )}
              </div>
              <ButtonComponent
                disabled={loadingAddcart && true}
                name={
                  loadingAddcart && "disabled" ? (
                    <LoadingComponent size="sm" />
                  ) : (
                    "Thêm giỏ hàng"
                  )
                }
                size="sm"
                color="success"
                onClick={onClickAddToCart}
              />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default BoxItem;
