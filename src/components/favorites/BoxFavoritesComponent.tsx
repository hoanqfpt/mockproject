import React from "react";
import ButtonComponent from "../../common-ui/ButtonComponent";
import { Trash } from "react-bootstrap-icons";
import { formatNumber } from "../../util/common";
import { deleteFavorites } from "../../store/favorites/action-creators";
import { useDispatch, useSelector } from "react-redux";
import LoadingComponent from "../../common-ui/LoadingComponent";
import { addToCartAPI } from "../../store/cart/action-creators";
import { toast } from "react-toastify";

interface IBoxFavoritesProps {
  nameProduct: string;
  desProduct: string;
  quantity: number;
  price: number;
  id: string;
  maUser: number;
  maSP: string;
  imageDefault: string;
}

function BoxFavoritesComponent(Props: IBoxFavoritesProps) {
  const {
    nameProduct,
    desProduct,
    quantity,
    price,
    imageDefault,
    id,
    maSP,
  } = Props;
  const dispatch = useDispatch();
  const loading = useSelector((state: any) => state.favoritesData.loading);
  const onClickDelete = () => {
    dispatch(deleteFavorites(id));
  };
  const onClickAddCart = () => {
    dispatch(
      addToCartAPI({
        name: nameProduct,
        price: price,
        imageDefault: imageDefault,
        maSP: maSP,
        quantity: quantity
      })
    );
    dispatch(deleteFavorites(id));
  };
  return (
    <React.Fragment>
      <div className="row shadow align-items-center mt-4 rounded">
        <div className="col-md-2">
          <img
            className="rounded w-50  shadow rounded"
            src={imageDefault}
            alt=""
          />
        </div>
        <div className="col-md-4 p-3 ">
          <h6>{nameProduct}</h6>
          <em>{desProduct}</em>
        </div>
        <div className="col-md-1">
          <div className="row align-content-center justify-content-center">
            {/* <ButtonComponent name="-" size="sm"color="white"/> */}
            <p className="my-0 w-25 text-center border rounded">{quantity}</p>
            {/* <ButtonComponent name="+" size="sm" color="white" /> */}
          </div>
        </div>
        <div className="col-md-2 ">
          <h6 className="m-0">{formatNumber(price)} VNĐ</h6>
        </div>
        <div className="col-md-2">
          <ButtonComponent
            name="Add to cart"
            size="md"
            color="success"
            onClick={onClickAddCart}
          />
        </div>
        <div className="col-md-1">
          {loading ? (
            <LoadingComponent size="sm" />
          ) : (
            <Trash
              color="red"
              size={20}
              style={{ cursor: "pointer" }}
              onClick={onClickDelete}
            />
          )}
        </div>
      </div>
    </React.Fragment>
  );
}
export default BoxFavoritesComponent;
