import React from "react";
import styles from "./LookBook.module.css";
import SliderComponent from "./SliderComponent";
import { Link } from "react-router-dom";

function LookBook() {
  return (
    <React.Fragment>
      <div>
        <div className={styles.wrapper}>
          <div className={styles.title}>
            <h1>The Blue Chic</h1>
            <p>Đẳng Cấp Quý Ông!</p>
            <a className="btn btn-dark">
              <Link to="/products">Shopping Now!</Link>
            </a>
          </div>

          <div className={styles.backgroundImage}>
            <img
              src="https://res.cloudinary.com/elvisheddy/image/upload/v1633871215/lookbook_image/image_11_zpsl5t.jpg"
              alt="image"
            />
          </div>
        </div>

        <div className={styles.mainSection}>
          <div className="row mt-5">
            <div className="col-md-8">
              <img
                src="https://res.cloudinary.com/elvisheddy/image/upload/v1633877775/lookbook_image/image_14_w6z5ss.jpg"
                alt="image"
                style={{ width: "100%" }}
              />
            </div>

            <div className="col-md-4">
              <h1 className={styles.header}>Sang Trọng</h1>
            </div>
          </div>

          <div className="row mt-5">
            <div className="col-md-4">
              <h1 className={styles.header}>Lịch Lãm</h1>
            </div>

            <div className="col-md-8">
              <img
                src="https://res.cloudinary.com/elvisheddy/image/upload/v1633877854/lookbook_image/image_17_tblf0l.jpg"
                alt="image"
                style={{ width: "100%" }}
              />
            </div>
          </div>

          <div className="row mt-5">
            <div className="col-md-8">
              <img
                src="https://res.cloudinary.com/elvisheddy/image/upload/v1633942014/lookbook_image/1_jugqgs.jpg"
                alt="image"
                style={{ width: "100%" }}
              />
            </div>

            <div className="col-md-4">
              <h1 className={styles.header}>Phong Cách</h1>
            </div>
          </div>

          <div className="row mt-5">
            <div className="col-md-6">
              <img
                src="https://res.cloudinary.com/elvisheddy/image/upload/v1633871215/lookbook_image/image_12_xz8ve6.png"
                alt="image"
                style={{ width: "100%" }}
              />
            </div>

            <div className="col-md-6">
              <img
                src="https://res.cloudinary.com/elvisheddy/image/upload/v1633871214/lookbook_image/image_13_asxo4d.jpg"
                alt="image"
                style={{ width: "100%" }}
              />
            </div>
          </div>

          <div className="mt-5">
            <h1 className="text-center">#TheBlueChicCollection</h1>
            <SliderComponent />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default LookBook;
