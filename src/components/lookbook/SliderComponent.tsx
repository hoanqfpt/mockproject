import React from "react";
import Slider from "react-slick";
import { data } from "./api";
export default function SliderComponent() {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
  };
  return (
    <React.Fragment>
      <Slider {...settings} className="w-100">
        {data.map((v, i) => {
          return (
            <div key={i} className="p-2">
              <img
                src={v.urlImage}
                alt="image_slider"
                style={{ width: "100%", height: "467px" }}
              />
            </div>
          );
        })}
      </Slider>
    </React.Fragment>
  );
}
