import React from "react";
import styles from "./OrderList.module.css";

const OrderList = () => {
  return (
    <React.Fragment>
      <section className={`mt-5 container ${styles["your-order"]}`}>
        <table className="table">
          <thead className="thead-dark text-center">
            <tr>
              <th>Sản phẩm</th>
              <th>Số lượng đã mua</th>
              <th>Tổng tiền</th>
            </tr>
          </thead>
          <tbody className="text-center">
            <tr>
              <td className="font-weight-bold">name 1</td>
              <td>3</td>
              <td>72</td>
            </tr>
            <tr>
              <td className="font-weight-bold">name 1</td>
              <td>3</td>
              <td>72</td>
            </tr>
          </tbody>
        </table>
      </section>
    </React.Fragment>
  );
};

export default OrderList;
