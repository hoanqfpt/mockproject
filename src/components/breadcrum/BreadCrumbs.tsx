import React from "react";
import { Link } from "react-router-dom";
import styles from "./BreadCrumbs.module.css";
// @ts-ignore
const Breadcrumbs = ({ crumbs }) => {
  if (crumbs.length <= 1) {
    return null;
  }

  return (
    <div className={styles.breadCrumb}>
      <div className="p-2">
      {/* @ts-ignore */}
      {crumbs.map(({ name, path }, key) =>
        key + 1 === crumbs.length ? (
          <span key={key}>{name}</span>
        ) : (
          <Link key={key} to={path}>
            {name}/
          </Link>
        )
      )}
      </div>
    </div>
  );
};

export default Breadcrumbs;
