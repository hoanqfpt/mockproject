import React from "react";
import styles from "./CSS/NewsPost.module.css";
function NewsPost(props: any) {
  const { value } = props;
  return (
    <React.Fragment>
      <div
        className="post-section row d-flex"
        style={{ paddingBottom: "40px" }}
      >
        <div className="col-md-6">
          <div className="post-image mb-4">
            <a href="#">
              <div className={styles.postImage}>
                <img src={value.url} />
              </div>
            </a>
          </div>
        </div>

        <div className="col-md-6">
          <div className={styles.postContent}>
            <p className="text-secondary">
              <time dateTime="2020-08-26T16:48:00Z">August 26, 2020</time>
            </p>
            <h2  className="mb-3">
              <a href="#" className="text-decoration-none text-dark">
                Easy Family Home Work Outs
              </a>
            </h2 >
            <p className="mb-4">
              Even though most of Utah has fully opened up – we’ve been having
              fun doing some quick family work outs...
            </p>
            <a href="#" className="btn btn-dark rounded-0 mb-5">
              Read More
            </a>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default NewsPost;
