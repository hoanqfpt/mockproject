import React from "react";
import styles from "./CSS/RecentPost.module.css";

function RecentPost(props: any) {
  const { value } = props;
  return (
    <React.Fragment>
      <div className={styles.recentPost}>
        <div>
          <a href="#">
            <img src={value.url} />
          </a>
        </div>

        <div className={styles.title}>
          <p>
            <time dateTime="2020-08-26T16:48:00Z">August 26, 2020</time>
          </p>
          <h6 className="mb-3">
            <a href="#" className="text-decoration-none text-dark">
              Easy Family Home Work Outs
            </a>
          </h6>
        </div>
      </div>
    </React.Fragment>
  );
}

export default RecentPost;
