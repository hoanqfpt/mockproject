import React from "react";
import styles from "./CSS/TrendingPost.module.css";

function TrendingPost(props: any) {
  const { value } = props;
  return (
    <React.Fragment>
      <div className="row" id={styles.post}>
        <div className="col-md-12">
          <div className="post-image mb-4">
            <a href="#">
              <div className={styles.image}>
                <img src={value.url} />
              </div>
            </a>
          </div>
        </div>

        <div className="col-md-12">
          <div className={styles.title}>
            <h2 className="mb-3">
              <a href="#" className="text-decoration-none text-dark">
                Easy Family Home Work Outs
              </a>
            </h2>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default TrendingPost;
