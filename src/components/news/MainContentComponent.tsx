import NewsPost from "./NewsPost";
import RecentPost from "./RecentPost";
import TrendingPost from "./TrendingPost";
import { api, trending } from "./data";

function MainContentComponent() {
  return (
    <main style={{ padding: "80px 0" }}>
      <div id="content-section">
        <div className="container-fluid pt-4">
          <div className="row">
            <div className="col-md-9 pr-lg-5">
              <div className="row">
                <div className="col-12 col-md-12">
                  {api.map((v: any, i: number) => {
                    return <NewsPost value={v} key={i} />;
                  })}
                </div>
              </div>
              <div className="pagination"></div>
            </div>
            <div className="col-md-3 sidebar-post">
              <div className="sidebar-post-wrap">
                <div className="block-small">
                  <h5 style={{ marginBottom: "30px" }}>Trending Post</h5>
                  {trending.map((v: any, i: number) => {
                    return <TrendingPost value={v} key={i} />;
                  })}
                </div>
                <div className="block-small">
                  <h5 style={{ marginBottom: "30px" }}>Trending Post</h5>

                  {api.map((v: any, i: number) => {
                    return <RecentPost value={v} key={i} />;
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default MainContentComponent;
