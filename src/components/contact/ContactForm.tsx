import React from "react";
import {
  Formik,
  Field,
  Form,
  FastField,
  FormikProps,
  FormikHelpers,
} from "formik";
import ButtonComponent from "../../common-ui/ButtonComponent";
import * as Yup from "yup";
import InputField from "../../common-ui/InputField";
import styles from "./CSS/ContactForm.module.css";
import { CONTACT_API } from "./contactapi";
import axios from "axios";
import { toast } from "react-toastify";
export interface IContactFormValue {
  name: string;
  email: string;
  message: string;
  phone: string;
}
function ContactForm() {
  const initialValues: IContactFormValue = {
    name: "",
    email: "",
    message: "",
    phone: "",
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Please enter your name"),
    email: Yup.string()
      .email("Invalid Email")
      .required("Please enter your email address"),
    message: Yup.string().required("Please enter your message"),
    phone: Yup.string().required("Please enter your phone number"),
  });
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={async (
          values,
          formikHelpers: FormikHelpers<IContactFormValue>
        ) => {
          try {
            const response = await axios.post(CONTACT_API + "/contact", values);
             toast.success("Cảm ơn bạn đã gửi liên hệ với chúng tôi!", {
               autoClose: 1500,
               className: "mt-5",
             });
            formikHelpers.resetForm();
          } catch (error: any) {
            if (error) {
              alert(error);
            }
          }
        }}
        validationSchema={validationSchema}
      >
        {(formikProps: FormikProps<IContactFormValue>) => {
          return (
            <Form className={styles.formContainer}>
              <FastField
                name="name"
                component={InputField}
                className="form-control"
                placeholder="Name"
              />
              <FastField
                name="email"
                component={InputField}
                className="form-control"
                placeholder="Email"
              />
              <FastField
                name="phone"
                component={InputField}
                className="form-control"
                placeholder="Phone Number"
              />
              <div className="form-group">
                <FastField
                  name="message"
                  type="text"
                  as="textarea"
                  className="form-control"
                  rows={5}
                  cols={50}
                  id={styles.textMessage}
                  placeholder="Message"
                />
              </div>
              <div className="mt-3">
                <ButtonComponent
                  type="submit"
                  name="Send"
                  color="info"
                  disabled={!formikProps.isValid || !formikProps.dirty}
                />
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}
export default ContactForm;
