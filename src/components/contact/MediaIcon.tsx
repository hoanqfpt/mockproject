import React from "react";
import {
  FacebookOutlined,
  InstagramOutlined,
  TwitterOutlined,
} from "@ant-design/icons";

function MediaIcon() {
  return (
    <React.Fragment>
      <div>
        <FacebookOutlined
          style={{ fontSize: "30px", color: "#63B4B8", marginRight: "10px" }}
        />
        <InstagramOutlined
          style={{ fontSize: "30px", color: "#63B4B8", marginRight: "10px" }}
        />
        <TwitterOutlined
          style={{ fontSize: "30px", color: "#63B4B8", marginRight: "10px" }}
        />
      </div>
    </React.Fragment>
  );
}

export default MediaIcon;
