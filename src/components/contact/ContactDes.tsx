import React from "react";

function ContactDes(props: any) {
  const { title } = props;
  return (
    <React.Fragment>
      <div
        className="contact-description"
        style={{ fontWeight: "bold", fontSize: "14px" }}
      >
        <p className="m-0" style={{ color: "#999", fontWeight: 500 }}>
          CONTACT
        </p>
        <h3 className="my-2" style={{ fontSize: "30px", color: "#000" }}>
          {title}
        </h3>
      </div>
    </React.Fragment>
  );
}

export default ContactDes;
