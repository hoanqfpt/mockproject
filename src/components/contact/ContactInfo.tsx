import React from "react";
import styles from "./CSS/ContactInfo.module.css";

function ContactInfo() {
  return (
    <React.Fragment>
      <div className={styles.address}>
        <p>123 Sky Tower, West 21th Street, Suite 721, NY</p>
        <p>
          <span className={styles.borderBottom}>+84 923 716 866</span>
        </p>
        <p>
          <a
            href="mailto:admin@adminstore.vn"
            target="_blank"
            className={styles.borderBottom}
          >
            admin@adminstore.vn
          </a>
        </p>
      </div>
    </React.Fragment>
  );
}
export default ContactInfo;
