import React from "react";
import ContactInfo from "./ContactInfo";
import ContactForm from "./ContactForm";
import ContactDes from "./ContactDes";
import MediaIcon from "./MediaIcon";

function ContactContainer() {
  return (
    <React.Fragment>
      <div className="container-fluid mt-5">
        <div className="row">
          <div className="col-md-6 pl-5 text-center">
            <ContactDes title="INFORMATION" />
            <ContactInfo />
            <MediaIcon />
          </div>
          <div className="col-md-3">
            <ContactForm />
          </div>
        </div>
        <div className="map text-center" style={{ marginTop: "120px" }}>
         
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.652232496002!2d105.80465941493217!3d21.006573086010377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac9f48774cbd%3A0x5e1510923e469dc4!2zTmfDtSAyMSBMw6ogVsSDbiBMxrDGoW5nLCBOaMOibiBDaMOtbmgsIFRoYW5oIFh1w6JuLCBIw6AgTuG7mWk!5e0!3m2!1svi!2s!4v1632911826310!5m2!1svi!2s"
            width="100%"
            height="500"
           
            loading="lazy"
          ></iframe>
        </div>
      </div>
    </React.Fragment>
  );
}

export default ContactContainer;
