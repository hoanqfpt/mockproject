import React from "react";
import { NavLink } from "react-router-dom";

import style from './Footer.module.css'

function Footer() {
  return (
    <React.Fragment>
      <footer className={style.footer}>
        <div className="container mt-5 pt-5">
          <div className="row">
            <div className="col-md-4">
              <h4>B L U E C H I C</h4>
              <p>
                Thương hiệu giày made in vietnam. Được thành lập từ năm 2021 với
                kinh nghiệm nhiều năm trong lĩnh vực sản xuất giày da. Chúng tôi
                mong muốn đem lại cho khách hàng những trải nghiệm tốt nhất.
              </p>
              <p>Phone: +84923716866</p>
              <p>Email: admin@adminstore.vn</p>
            </div>

            <div className="col-md-4 d-none d-md-block d-lg-block">
              <h6>Dịch vụ khách hàng</h6>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <NavLink to="/news">Tin tức</NavLink>
                </li>
                <li className="list-group-item">
                  <NavLink to="/contact">Liên hệ</NavLink>
                </li>
                <li className="list-group-item">Khuyến mãi</li>
                <li className="list-group-item">Adam's Video</li>
                <li className="list-group-item">Ưu đãi đối tác Adam</li>
              </ul>
            </div>

            <div className="col-md-4 d-none d-md-block d-lg-block">
              <h6>Nhóm sản phẩm</h6>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">Giày da</li>
                <li className="list-group-item">Giày thể thao</li>
                <li className="list-group-item">Giày công sở</li>
                <li className="list-group-item">Giày thời trang</li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </React.Fragment>
  );
}
export default Footer;
