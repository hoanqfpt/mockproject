import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_API,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: "mockproject-64d1c",
  storageBucket: "mockproject-64d1c.appspot.com",
  messagingSenderId: "658135569355",
  appId: "1:658135569355:web:5d012744b200d5c48c1c4e",
  measurementId: "G-XLCHHVKCH8"
};

// Initialize Firebase
// const appFirebase = 
firebase.initializeApp(firebaseConfig);
// export const firebaseAuth = firebase.auth();
// export const firestore = firebase.firestore()
// const analytics = getAnalytics(appFirebase);
 export default firebase;