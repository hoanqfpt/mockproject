export const formatNumber = (value: any) => {
    const num= new Intl.NumberFormat("vn-VN", {
    maximumSignificantDigits: 3,
  }).format(value);
  return num;
};