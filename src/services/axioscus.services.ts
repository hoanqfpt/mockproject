import axios, { AxiosRequestConfig, Method } from "axios";
import { toast } from "react-toastify";

const axiosApi = axios.create({
  baseURL: process.env.REACT_APP_API_URL_CUS,
});
axios.interceptors.request.use(
  function (config) {
    // config.headers.Authorization = "Bearer " + localStorage.getItem("accessToken");
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  function (response) {
    if (response.status == 200) {
      toast.success(response.data.message, {
        autoClose: 1500,
        className: "mt-5",
      });
    }
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default async function callApiCus(
  url: string,
  method: Method,
  config?: AxiosRequestConfig
) {
  try {
    const response = await axiosApi({ url, method, ...config });
    return response;
  } catch (error) {
    if (error) {
    }
  }
}

export async function get(url: string, config: any) {
  return await axiosApi
    .get(url, { ...config })
    .then((response) => response.data);
}

export async function post(url: string, data: any, config: any) {
  return await axiosApi
    .post(url, { ...data }, { ...config })
    .then((response) => response.data);
}

export async function put(url: string, data: any, config: any) {
  return axiosApi
    .put(url, { ...data }, { ...config })
    .then((response) => response.data);
}

export async function del(url: string, config: any) {
  return await axiosApi
    .delete(url, { ...config })
    .then((response) => response.data);
}

