interface ILoading {
  size?:string;
}

function LoadingComponent(Props: ILoading) {
  const {size} = Props;
  return (
    <div className="d-flex justify-content-center align-items-center ">
      <div
        className={`spinner-border spinner-border-${size}  text-warning`}
        role="status"
      ></div>
    </div>
  );
}

export default LoadingComponent;
