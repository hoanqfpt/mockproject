import { useDispatch} from "react-redux";
import { addToCart, addToCartAPI } from "../store/cart/action-creators";
import { toast } from "react-toastify";
import ButtonComponent from "./ButtonComponent";

interface IAddToCartProps {
  name: string;
  price: number;
  color: string;
  imageDefault: string;
  id: string;
  quantity: number;
  content: React.ReactElement | string;
  buttonColor?: string;
  buttonSize?: "sm" | "md" | "lg";
  className?: string;
}

const AddToCartButton = (Props: IAddToCartProps) => {
  const {
    name,
    price,
    color,
    imageDefault,
    id,
    quantity,
    content,
    buttonColor,
    buttonSize,
    className,
  } = Props;
  const cartItem: any = {
    name: name,
    price: price,
    color: color,
    imageDefault: imageDefault,
    maSP: id,
    quantity: quantity,
  };
  const dispatch = useDispatch();
  const onClickAddToCart = () => {
    const localCart: string | null = localStorage.getItem("cartItem");
    const listCart: string[] = JSON.parse(
      localCart !== null ? localCart : "[]"
    );
    const check = listCart.filter((item: any) => {
      return item.maSP === id;
    });
    if (check.length > 0) {
      toast.warn("Sản phẩm đã có trong giỏ hàng", {
        autoClose: 2000,
        className: "mt-5",
      });
      return;
    }

    listCart.push(cartItem);
    const action = addToCart(listCart);
    dispatch(action);
    localStorage.setItem("cartItem", JSON.stringify(listCart));
    toast.success("Đã được thêm vào giỏ hàng", {
      autoClose: 2000,
      className: "mt-5",
    });
    const maUser = localStorage.getItem("idUser");
    if (maUser !== null) {
      const action = addToCartAPI(cartItem);
      dispatch(action);
    }
  };
  return (
    <ButtonComponent
      name={content}
      onClick={onClickAddToCart}
      color={buttonColor}
      size={buttonSize}
      className={className}
    />
  );
};

export default AddToCartButton;
