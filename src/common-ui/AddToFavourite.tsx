import React from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { addFavorites } from "../store/favorites/action-creators";
import ButtonComponent from "./ButtonComponent";

interface IAddToFavouriteProps {
  name: string;
  price: number;
  color: string;
  imageDefault: string;
  id: string;
  quantity?: number;
  size: string;
  content: React.ReactElement | string;
  buttonColor?: string;
  buttonSize?: "sm" | "md" | "lg";
  className?: string;
}

const AddToFavourite = (Props: IAddToFavouriteProps) => {
  const {
    name,
    price,
    color,
    imageDefault,
    id,
    size,
    quantity,
    content,
    buttonColor,
    buttonSize,
    className,
  } = Props;
  const dispatch = useDispatch();
  const onClickAddToFavorites = () => {
    const maUser = localStorage.getItem("idUser");
    if (maUser === null) {
      toast.warn("Vui lòng đăng nhập!", {
        autoClose: 1500,
        className: "mt-5",
      });
      return;
    }
    dispatch(
      addFavorites({
        nameProduct: name,
        price: price,
        color: color,
        image: imageDefault,
        maSP: id,
        desProduct: `${color.length} colour - ${size.length} size`,
        maUser: Number(maUser),
      })
    );
  };
  return (
    <ButtonComponent
      name={content}
      onClick={onClickAddToFavorites}
      color={buttonColor}
      size={buttonSize}
      className={className}
    />
  );
};

export default AddToFavourite;
