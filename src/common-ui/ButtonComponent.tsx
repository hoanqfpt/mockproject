import React, { FunctionComponent, FunctionComponentFactory, ReactElement } from "react";
import { Button } from "reactstrap";
interface IButtonComponentProps {
  name: string | React.ReactElement;
  color?: string;
  type?: any;
  onClick?: () => void;
  // display?: string;
  disabled?: any;
  outline?:boolean;
  size?: "sm" | "md" | "lg";
  active?:boolean;
  className?: string;
}

function ButtonComponent(Props: IButtonComponentProps) {
  const onClick = () => {
    if (Props.onClick !== undefined) {
      Props.onClick();
    }
  };
  return (
    <React.Fragment>
      <Button
        type={Props.type}
        color={Props.color}
        outline={Props.outline}
        onClick={onClick}
        disabled = {Props.disabled}
        size= {Props.size}
        active={Props.active}
        className = {Props.className}
      >
        {Props.name}
      </Button>
    </React.Fragment>
  );
}

export default ButtonComponent;
