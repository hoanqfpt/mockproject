// import React from "react";
import {Divider} from 'antd'

interface IDividerComponetnProps {
  name?: string;
  className?: string;
  type?: "horizontal" | "vertical";
  align?: "left" | "right" | "center" | undefined;
  dashed?: boolean;
  plain? : boolean;
}

function DividerComponent(Props: IDividerComponetnProps) {
    const {name,className,type,align,dashed,plain} = Props;
  return (
      <Divider dashed={dashed} plain={plain} className={className} type={type} orientation={align} >{name && name} </Divider>
  );
}

export default DividerComponent;
