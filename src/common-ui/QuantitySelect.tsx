import React from "react";
import ButtonComponent from "./ButtonComponent";
import { useDispatch } from "react-redux";
import styles from '../components/products/DetailComponent.module.css'
import { addToCart } from "../store/cart/action-creators";
import { CART } from "../components/cart/CartItem";

interface IQuantitySelectProps {
  quantity: number;
  onIncrease: () => void;
  onDecrease: () => void;
}

const QuantitySelect = (Props: IQuantitySelectProps) => {
  const { quantity, onIncrease, onDecrease } = Props;

  return (
    <div className="btn-group">
      <ButtonComponent name="-" size="sm" color="dark" onClick={onDecrease} />
      <div className={`my-0 ${styles.quantityContain}`}><span>{quantity}</span></div>
      <ButtonComponent name="+" size="sm" color="dark" onClick={onIncrease} />
    </div>
  );
};

export default QuantitySelect;
