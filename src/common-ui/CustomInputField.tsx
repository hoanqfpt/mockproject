import React, { FC } from "react";
import { ErrorMessage, Field, FieldConfig } from "formik";
import { FormFeedback, FormGroup, Input, Label } from "reactstrap";

interface IInputFiedlProps extends FieldConfig {
  className?: string;
  label?: string;
  required?: boolean;
  error?: string;
  placeholder?: string;
}
const CustomInputField: FC<IInputFiedlProps> = ({
  required = false,
  label = "",
  className = "",
  name,
  error = false,
  placeholder = "",
  ...rest
}) => {
  return (
    <React.Fragment>
      <FormGroup>
        {label && <Label htmlFor={name}>{label}</Label>}
        <Field
          name={name}
          {...rest}
          placeholder={placeholder ? placeholder : ""}
          className={`form-control ${className}`}
        />
      </FormGroup>
    </React.Fragment>
  );
};
export default CustomInputField;
