import { signUp } from "./store/user/action-creators";
import React, { Component } from "react";
import Home from "./pages/Home";
import About from "./pages/About";
import Products from "./pages/Products";
import ProductDetail from "./components/products/DetailComponent";
import News from "./pages/News";
import Contact from "./pages/Contact";
import AuthenContainer from "./components/login/AuthenContainer";
import SignUp from "./components/login/SignUp";
import ResetPassword from "./components/login/ResetPassword";
import ConfirmPassword from "./components/login/ConfirmPassword";
import Account from "./pages/Account";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";
import CheckOutContainer from "./components/checkoutpage/CheckOutContainer";
import OrderList from "./components/order/OrderList";
import AdminPage from "./pages/admin/AdminPage";
export default [
  { path: "/", name: "Home", Compoent: Home },
  // { path: "/home", name: "Home", Compoent: Home },
  { path: "/about", name: "Giới Thiệu", Component: About },
  { path: "/products", name: "Sản Phẩm", Component: Products },
  { path: "/products/:id", name: "Chi Tiết", Component: ProductDetail },
  { path: "/news", name: "Tin Tức", Component: News },
  { path: "/contact", name: "Liên Hệ", Component: Contact },
  {
    path: "/login",
    name: "Đăng Nhập",
    Component: AuthenContainer,
    authen: true,
  },
  { path: "/signup", name: "Đăng Ký", Component: SignUp },
  { path: "/reset-password", name: "Reset Mật Khẩu", Component: ResetPassword },
  {
    path: "/confirm-password/:forgotToken",
    name: "Xác Nhận Mật Khẩu",
    Component: ConfirmPassword,
  },
  { path: "/account", name: "Tài Khoản", Component: Account },
  { path: "/cart", name: "Giỏ hàng", Component: Cart },
  { path: "/favorites", name: "Yêu thích", Component: Favorites },
  { path: "/checkout", name: "Thanh Toán", Component: CheckOutContainer },
  { path: "/order", name: "Lịch sử mua hàng", Component: OrderList },
  { path: "/admin", name: "Admin", Component: AdminPage },
];
